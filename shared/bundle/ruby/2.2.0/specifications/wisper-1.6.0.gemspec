# -*- encoding: utf-8 -*-
# stub: wisper 1.6.0 ruby lib

Gem::Specification.new do |s|
  s.name = "wisper"
  s.version = "1.6.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Kris Leech"]
  s.date = "2014-10-25"
  s.description = "pub/sub for Ruby objects"
  s.email = ["kris.leech@gmail.com"]
  s.homepage = "https://github.com/krisleech/wisper"
  s.licenses = ["MIT"]
  s.rubygems_version = "2.4.6"
  s.summary = "pub/sub for Ruby objects"

  s.installed_by_version = "2.4.6" if s.respond_to? :installed_by_version
end
