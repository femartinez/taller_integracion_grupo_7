Rails.application.routes.draw do

  # routes e-commerce
  get 'e_commerce' => 'e_commerce#index'
  get 'e_commerce_list_products' => 'e_commerce#list_products'
  post 'e_commerce_add_product_to_cart' => 'e_commerce#add_product_to_cart'
  get 'e_commerce_show_cart' => 'e_commerce#show_cart'
  get 'e_commerce/confirm_order'
  post 'e_commerce_pay' => 'e_commerce#pay'
  post 'generar_pedido' => 'logic#generar_pedidos'
  get 'e_commerce_pay_success' => 'e_commerce#pay_success'
  get 'e_commerce_pay_cancel' => 'e_commerce#pay_canceled'
  get 'e_commerce/ship'
  get 'e_commerce_delete_from_cart' => 'e_commerce#delete_product_from_cart'
  get 'about_us' => 'e_commerce#us'
  get 'contact' => 'e_commerce#contact'
  get 'work' => 'e_commerce#work'


  devise_for :users
  get 'ftp/connection'

  # You can have the root of your site routed with "root"
  root 'logic#homepage'
  get 'admin' => 'logic#bodega_status'
  
  get 'admin/cantidad_ventas' => 'logic#cantidad_ventas'
  get 'admin/reportes' => 'logic#reportes'
  get 'admin/costos' => 'logic#costos'
  get 'admin/calidad_servicio' => 'logic#calidad_servicio'
  get 'admin/montos_facturados' => 'logic#montos_facturados'


  get 'dashboards_menu' => 'logic#dashboards'

  get'generar_pedido' => 'logic#generar_pedidos'

 namespace :api, defaults: {format: 'json'} do
    scope module: :v1 do
      #Métodos para registro y token
      #Registrar grupo
       post 'register_group' => 'b2b#create_group'
      #Obtener token
       get 'get_token' => 'b2b#get_token'
      #Métodos para orden de compra
       post 'create_order' => 'b2b#create_order'
       delete 'canceled_order' => 'b2b#canceled_order'
       put 'accepted_order' => 'b2b#accepted_order'
       put 'rejected_order' => 'b2b#rejected_order'
      #Métodos para las facturas
       post 'issued_invoice' => 'b2b#issued_invoice'
       put 'invoice_paid' => 'b2b#invoice_paid'
       put 'rejected_invoice' => 'b2b#rejected_invoice'
      #Documentacion
       get 'documentation' => 'b2b#documentation', defaults: {format: 'html'}
      #
       get 'bank_account' => 'b2b#bank_account'
    end
  end
  #Redirects
  get 'b2b/documentation', to: redirect('api/documentation')

  #Canal internacional
  get 'ftp/connection' => 'ftp#connection'

  get 'read_xml' => 'ftp#read_xml'

  #Instagram
  get "/instagram" => "instagram#autorizacion", as: "autorizacion"
  post "/instagram" => "instagram#obtener", as: "obtener"

  #Logic Controller
  get 'oc/validate_order' => 'logic#validar_OC', defaults: {format: 'json'}
  get 'bodega/despejar_recepcion' => 'logic#despejar_recepcion', defaults: {format: 'json'}
  get 'test_api_consumer' => 'logic#test_connection_with_systems', defaults: {format: 'html'}
  get 'verificar_productos' => 'logic#verificar_stock_productos', defaults: {format: 'html'}
  get 'bodega_status' => 'logic#bodega_status', defaults: {format: 'html'}
  get 'oc_externa' => 'logic#oc_externa', defaults: {format: 'html'}
  get 'bodega_status_refresh' => 'logic#bodega_status_refresh', defaults: {format: 'html'}
  get 'producir' => 'logic#producir', defaults: { format: 'html' }
  post 'producir' => 'logic#producir_send', defaults: { format: 'html' }
  get 'fabrica'=> 'logic#fabrica', defaults: {format: 'html'}


  #Instagram
  #get 'promociones' => 'instagram#home', defaults: {format: 'html'}
  #post 'promociones' => 'instagram#home', defaults: {format: 'html'}

end
