# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150630135036) do

  create_table "almacens", force: :cascade do |t|
    t.string   "tipo"
    t.integer  "totalSpace"
    t.integer  "usedSpace"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "almacen_id"
  end

  create_table "api_connections", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "api_consumer_g1s", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "api_consumer_g2s", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "api_consumer_g3s", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "api_keys", force: :cascade do |t|
    t.integer  "group_id"
    t.string   "access_token"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "banks", force: :cascade do |t|
    t.string   "account_id"
    t.string   "tipo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority"

  create_table "e_commerces", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "fabricas", force: :cascade do |t|
    t.string   "sku"
    t.integer  "cantidad"
    t.string   "status"
    t.datetime "fecha_despacho"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "ftps", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "groups", force: :cascade do |t|
    t.string   "username"
    t.string   "password"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "grupo5s", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "grupo6s", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "grupo8s", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "invoices", force: :cascade do |t|
    t.string   "invoice_id"
    t.datetime "creation_date"
    t.string   "supplier"
    t.string   "client"
    t.integer  "net_amount"
    t.integer  "tax"
    t.integer  "total_value"
    t.string   "payment_status"
    t.datetime "payment_date"
    t.string   "order_id"
    t.string   "reject"
    t.string   "cancel"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "transaction_id"
  end

  create_table "item_movements", force: :cascade do |t|
    t.datetime "date"
    t.integer  "item_id"
    t.string   "origen"
    t.string   "destino"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "items", force: :cascade do |t|
    t.string   "item_id"
    t.integer  "almacen_id"
    t.string   "sku"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "tipo"
    t.integer  "order_id"
    t.integer  "reserved"
    t.integer  "in_warehouse"
  end

  create_table "line_items", force: :cascade do |t|
    t.integer  "shop_cart_id"
    t.integer  "sku_id"
    t.integer  "quantity"
    t.float    "unit_price"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "status"
  end

  create_table "orders", force: :cascade do |t|
    t.string   "cliente"
    t.string   "proveedor"
    t.string   "sku"
    t.string   "estado"
    t.string   "fechaEntrega"
    t.integer  "precioUnitario"
    t.integer  "cantidad"
    t.string   "canal"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "order_id"
    t.string   "notas"
  end

  create_table "prices", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "promotions", force: :cascade do |t|
    t.integer  "sku_id"
    t.integer  "price"
    t.datetime "starts_at"
    t.datetime "ends_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "code"
  end

  create_table "shop_carts", force: :cascade do |t|
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "skus", force: :cascade do |t|
    t.integer  "_id"
    t.string   "nombre"
    t.string   "descripcion"
    t.integer  "materia_prima_id"
    t.string   "is_output"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "social_networks", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "transactions", force: :cascade do |t|
    t.string   "transaction_id"
    t.integer  "amount"
    t.datetime "date"
    t.string   "emisor"
    t.string   "receptor"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
