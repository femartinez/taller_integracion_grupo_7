class CreatePromotions < ActiveRecord::Migration
  def change
    create_table :promotions do |t|
      t.integer :sku_id
      t.integer :price
      t.datetime :starts_at
      t.datetime :ends_at

      t.timestamps null: false
    end
  end
end
