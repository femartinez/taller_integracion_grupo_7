class CreateSkus < ActiveRecord::Migration
  def change
    create_table :skus do |t|
      t.integer :_id
      t.string :nombre
      t.string :descripcion
      t.integer :materia_prima_id
      t.string :is_output

      t.timestamps null: false
    end
  end
end
