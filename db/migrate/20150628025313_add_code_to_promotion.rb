class AddCodeToPromotion < ActiveRecord::Migration
  def change
    add_column :promotions, :code, :string
  end
end
