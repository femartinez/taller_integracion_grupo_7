class AddInWarehouseToItem < ActiveRecord::Migration
  def change
    add_column :items, :in_warehouse, :integer
  end
end
