class CreateBanks < ActiveRecord::Migration
  def change
    create_table :banks do |t|
      t.string :account_id
      t.string :tipo

      t.timestamps null: false
    end
  end
end
