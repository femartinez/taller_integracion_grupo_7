class CreateFabricas < ActiveRecord::Migration
  def change
    create_table :fabricas do |t|
      t.string :sku
      t.integer :cantidad
      t.string :status
      t.datetime :fecha_despacho

      t.timestamps null: false
    end
  end
end
