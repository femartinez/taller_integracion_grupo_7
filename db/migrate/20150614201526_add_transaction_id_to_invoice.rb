class AddTransactionIdToInvoice < ActiveRecord::Migration
  def change
    add_column :invoices, :transaction_id, :integer
  end
end
