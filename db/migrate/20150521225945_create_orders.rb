class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :client
      t.string :provider
      t.string :sku
      t.string :status
      t.string :DeliveryDate
      t.integer :price
      t.integer :amount
      t.string :channel

      t.timestamps null: false
    end
  end
end
