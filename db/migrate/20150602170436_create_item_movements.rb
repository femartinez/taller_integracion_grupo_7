class CreateItemMovements < ActiveRecord::Migration
  def change
    create_table :item_movements do |t|
      t.datetime :date
      t.integer :item_id
      t.string :origen
      t.string :destino

      t.timestamps null: false
    end
  end
end
