class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :item_id
      t.string :almacen_id
      t.string :sku

      t.timestamps null: false
    end
  end
end
