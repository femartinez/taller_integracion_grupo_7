class CreateAlmacens < ActiveRecord::Migration
  def change
    create_table :almacens do |t|
      t.string :tipo
      t.integer :totalSpace
      t.integer :usedSpace

      t.timestamps null: false
    end
  end
end
