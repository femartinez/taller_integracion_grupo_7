class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.string :invoice_id
      t.datetime :creation_date
      t.string :supplier
      t.string :client
      t.integer :net_amount
      t.integer :tax
      t.integer :total_value
      t.string :payment_status
      t.datetime :payment_date
      t.string :oc_id
      t.string :reject
      t.string :cancel

      t.timestamps null: false
    end
  end
end
