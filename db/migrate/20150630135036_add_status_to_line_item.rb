class AddStatusToLineItem < ActiveRecord::Migration
  def change
    add_column :line_items, :status, :integer
  end
end
