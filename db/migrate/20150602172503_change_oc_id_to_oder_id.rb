class ChangeOcIdToOderId < ActiveRecord::Migration
  def change
  	rename_column :invoices, :oc_id, :order_id
  end
end
