class CreateLineItems < ActiveRecord::Migration
  def change
    create_table :line_items do |t|
      t.integer :shop_cart_id
      t.integer :sku_id
      t.integer :quantity
      t.float :unit_price

      t.timestamps null: false
    end
  end
end
