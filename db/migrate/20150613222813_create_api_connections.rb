class CreateApiConnections < ActiveRecord::Migration
  def change
    create_table :api_connections do |t|

      t.timestamps null: false
    end
  end
end
