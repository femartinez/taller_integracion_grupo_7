class FixOrderColumnNames < ActiveRecord::Migration
  def change
  	rename_column :orders, :client, :cliente
  	rename_column :orders, :provider, :proveedor
  	rename_column :orders, :status, :estado
  	rename_column :orders, :DeliveryDate, :fechaEntrega
  	rename_column :orders, :price, :precioUnitario
  	rename_column :orders, :amount, :cantidad
  	rename_column :orders, :channel, :canal
  end
end
