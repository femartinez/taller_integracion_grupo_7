class CreateSocialNetworks < ActiveRecord::Migration
  def change
    create_table :social_networks do |t|

      t.timestamps null: false
    end
  end
end
