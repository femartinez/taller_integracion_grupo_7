class AddReservedToItems < ActiveRecord::Migration
  def change
    add_column :items, :reserved, :integer
  end
end
