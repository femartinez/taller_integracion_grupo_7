class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.string :transaction_id
      t.integer :amount
      t.datetime :date
      t.string :emisor
      t.string :receptor

      t.timestamps null: false
    end
  end
end
