# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

sku_huevo = Sku.create(_id: 2, nombre: 'Huevo', descripcion: 'Crea las mejores recetas con huevos de excelente calidad', is_output: 1)
sku_huevo = Sku.create(_id: 3, nombre: 'Maíz', descripcion: 'Crea las mejores recetas con huevos de excelente calidad', is_output: 1)
sku_huevo = Sku.create(_id: 11, nombre: 'Margarina', descripcion: 'Crea las mejores recetas con huevos de excelente calidad', is_output: 1)
sku_huevo = Sku.create(_id: 40, nombre: 'Queso', descripcion: 'Crea las mejores recetas con huevos de excelente calidad', is_output: 1)
sku_huevo = Sku.create(_id: 41, nombre: 'Suero de leche', descripcion: 'Crea las mejores recetas con huevos de excelente calidad', is_output: 1)
