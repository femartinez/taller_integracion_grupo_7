require 'test_helper'

class ReportesControllerTest < ActionController::TestCase
  test "should get ventas" do
    get :ventas
    assert_response :success
  end

  test "should get costos" do
    get :costos
    assert_response :success
  end

end
