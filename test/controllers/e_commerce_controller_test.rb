require 'test_helper'

class ECommerceControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get list_products" do
    get :list_products
    assert_response :success
  end

  test "should get add_product_to_cart" do
    get :add_product_to_cart
    assert_response :success
  end

  test "should get show_cart" do
    get :show_cart
    assert_response :success
  end

  test "should get confirm_order" do
    get :confirm_order
    assert_response :success
  end

  test "should get pay" do
    get :pay
    assert_response :success
  end

  test "should get ship" do
    get :ship
    assert_response :success
  end

end
