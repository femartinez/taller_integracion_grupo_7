class FtpController < ApplicationController

  require 'net/sftp'
  require 'json'
  before_action 'read_xml'

  def connection
    Ftp.connect
    redirect_to :root
  end


  def read_xml
    #Leemos los XML que estan en la carpeta pedidos. Cambiar esta ubicacion de acuerdo a donde se esta ejecutando rails
    #if Rails.env == 'development'
     # @path = Rails.root.to_s + '/shared/tmp/pedidos/'
    #else
      @path = '/home/administrator/apps/taller_integracion_grupo_7/shared/tmp/pedidos/'
    #end
    @files = Dir.entries(@path)
    @xmls = Array.new
    @oc = Array.new
    @files.each do |filename|
      next if filename == '.' || filename == '..'
      @xmls.push(Nokogiri::XML(File.open(@path + filename.to_s)))
    end
    if params[:q] != nil and params[:opts] != nil
      @xmls.each do |x|
        if x.xpath('//' + params[:opts]).to_s.partition('>').last.partition('<').first == params[:q]
          @oc.push(x)
        end
      end
      if @oc.empty? == true
        @oc = 'not_found'
      end
    end
    if params[:get_order_oc] == "OC"
      @query = JSON.parse Order.get(params[:q]).body
    end
      #@reserved = Order.get(params[:q]).items
    #@input = Order.items.input
    #@output = Order.items.output
  end
end

