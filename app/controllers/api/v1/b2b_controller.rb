class Api::V1::B2bController < ApplicationController
	require 'json'
	protect_from_forgery with: :null_session
	before_filter :authorize, except:[:create_group, :documentation, :get_token]

	before_action :group_params, only: [:create_group, :get_token]
	before_action :order_params, only: [:create_order, :accepted_order, :rejected_order, :canceled_order]
	before_action :invoice_params, only:[:issued_invoice, :invoice_paid, :rejected_invoice] 

	# Este método recibe los parametros username y password para crear un usuario de grupo
	# Si el grupo es creado satisfactoriamente, se devuelve el token.
	# Errores: 
	#  - Falta el usuario o la contraseña
	#  - EL nombre de grupo ya existe
	def create_group
		@group = Group.new(@params)
		respond_to do |format|
			if @group.save
				format.json {render json: {token: @group.api_key.access_token},status:200}
			else
				if @group.errors.messages[:username] || @group.errors.messages[:password]
					 if @group.errors.messages[:username] == '["Missing parameters"]' || @group.errors.messages[:password]
					 	format.json {render json: {description: 'Missing parameters'},status:400}
					 else
					 	format.json {render json: {description: 'Group already exists'},status:409}
					 end
				else
					format.json {render json: {description: @group.errors}, status:500 }
				end
			end
		end
	end
	
	# Este método recibe el usuario y contraseña de un grupo y devuelve el token asociado al grupo
	def get_token
		respond_to do |format|
			if @params[:username] && @params[:password]
				@group = Group.where(username: @params[:username]).first
				if @group
					if @group.verify_password(@params[:password])
						format.json {render json: {token: @group.api_key.access_token},status:200}
					else
						format.json {render json: {description: 'Invalid password'},status:404}
					end
				else
					format.json {render json: {description: 'Group not found'},status:404}
				end
			else
				format.json {render json: {description: 'Missing parameters'},status:400}
			end
		end
	end

	# Métodos orden de compra
	# Antes de todos estos métodos se valida el token y se setea la variable @order con el id

	#Este método avisa que se generó una orden de compra
	def create_order
		#Aquí debemos gatillar todo el proceso de producción
		#Primero se debe obtener la información de la orden de compra a través de la aplicación JBOSS
		#Luego hay que validar la información

		#Aquí se deben gatillar procesos de manera ASINCRONA
		#Si la información es válida, gatillar proceso de fabricación, factura, etc
		#Si la información no es válida se debe rechazar la factura en el sistema de facturas
		# y posteriormente informar al grupo solicitante que la orden ha  sido rechazada
		# http://www.toptal.com/ruby-on-rails/the-publish-subscribe-pattern-on-rails
		# if params[:answer]
		# 	@request = params[:answer]
		# 	respond_to do |format|
		# 		if @request != "nil"
		# 			check_order
		# 			format.json {render json: {order_id: @request },status:200}
		# 		else
		# 			format.json {render json: { description: "Missing parameter" }, status:400}
		# 		end
		# 	end
		# else
		# 	redirect_to create_order_path(:cliente => params[:cliente],:proveedor => params[:proveedor],:sku => params[:sku],:fechaEntrega => params[:fechaEntrega],:precioUnitario => params[:precioUnitario],:cantidad => params[:cantidad],:notas => params[:notas], :canal => "b2b")
		# end

		respond_to do |format|
			format.json {render json: {order_id: @order[:order_id]},status:200}
		end
		
		if (check_order(@order[:order_id]))
			LogicController.start_b2b_process(@order[:order_id])
		end
		
	end
	
	#Metodo para checkear la orden de compra una vez que se ha verificado que se recibio satisfactoriamente el order_id
	#Falta verifiacar cosas
	def check_order(order_id)
		order = Order.get(order_id)
		if order.code == 400
			return false
		else
			return true
		end
	end
		#Luego analizar la factibilidad inicial de la OC con el response
		# Hay productos suficientes en stock?
		# =>	Aceptar orden (Ocupando el metodo de la api_consumer_controller)
		
		# Si no hay productos en stock:
		# =>Hay insumos para fabricar lo que se nos pide?
		# =>Se alcanza a hacer la orden en el plazo?
		# =>	Aceptar orden (Ocupando el metodo de la api_consumer_controller)
		
	#Este método avisa que una orden de compra fue aceptada
	def accepted_order
		respond_to do |format|
			format.json {render json: {order_id: check_order(@order[:order_id])},status:200}
		end
	end

	#Este método avisa que una orden de compra fue rechazada
	def rejected_order
		#Se deben lanzar eventos para verificar porque fue rechazada
		#Buscar otros proveedores o llegar a acuerdo con el grupo en cuestión
		respond_to do |format|
			format.json {render json: {order_id: check_order(@order[:order_id])},status:200}
		end
	end

	#este método avisa que una orden de compra fue cancelada
	def canceled_order
		#Detener proceso de producción
		respond_to do |format|
			format.json {render json: {order_id: check_order(@order[:order_id])},status:200}
		end
	end


	# Métodos facturas
	# Antes de todos los métodos de facturas (invoice) se setea la variable @invoice con el
	# id de la factura entregado en el request y se verifica el token

	# Este método avisa que se genero una factura
	def issued_invoice
		#debido a que no estan las rutas para el sistema de facturas no podemos verificar si existen o no
		respond_to do |format|
			format.json {render json: {invoice_id: @invoice[:invoice_id]},status:200}
		end
	end
	# Este método avisa que una factura se pago
	def invoice_paid
		respond_to do |format|
			format.json {render json: {invoice_id: @invoice[:invoice_id]},status:200}
		end
		
		LogicController.b2b_process2(@invoice[:invoice_id])
	end
	# Este método avisa que una factura fue rechazada
	def rejected_invoice
		respond_to do |format|
			format.json {render json: {invoive_id: @invoice[:invoice_id]},status:200}
		end
	end

	#################################################################

	#Este metodo es para la página estática de la documentación
	def documentation
	end

	private
	def invoice_params
		@invoice = params.permit(:invoice_id, :format)
		unless @invoice[:invoice_id] 
			respond_to do |format|
				format.json {render json: {description: 'Missing parameter invoice_id'},status: 400}
			end
		end
	end
	def order_params
		@order = params.permit(:order_id, :format)
		unless @order[:order_id] 
			respond_to do |format|
				format.json {render json: {description: 'Missing parameter order_id'},status: 400}
			end
		end
	end
	def group_params
		#En caso de que vengan en el header bajo estos parametros
		username = request.headers.env["HTTP_USERNAME"]
		password = request.headers.env["HTTP_PASSWORD"]
		@params = params.permit(:username, :password)
		if username
			@params[:username] = username
		end
		if password
			@params[:password] = password			
		end
	end
	def token
		@token = request.headers[:authorization]
	end

	#Este metodo ve si el token provisto es valido, aún no esta probado
	def authorize
		token #Seteamos la variable @token
		if ApiKey.where(access_token: @token).first
			return true
		else
			respond_to do |format|
				format.json {render json: {description: 'Authentication failed'},status: :unauthorized}
			end
		end
	end
end
