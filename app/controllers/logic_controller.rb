class LogicController < ApplicationController

  require 'date'

  def homepage
  end
  
  def generar_pedido
    #Setear precio unitario canal y notas por defecto
    precio_unitario = Price.get_price(sku,DateTime.current)
    canal = 'b2b'
    notas = 13
    
    order_created = Order.create_order( params[:sku].to_i, params[:cantidad].to_i, precio_unitario, params[:fecha_entrega], params[:proveedor].to_s, canal, notas)
    order_id = order_created.parsed_response['_id']
    p order_created
    ApiConnection.create_order(order_id,proveedor)
  end
  
  def self.generar_pedido_example
    sku = 3
    cantidad = 15
    fecha_entrega = DateTime.current + 6.days
    proveedor = 'grupo7'
    generar_pedido(sku,cantidad,fecha_entrega,proveedor)
  end


  def self.validar_OC(order_id)
      order_accepted = false #boolean que indicará si se aceptó la orden o no
      
      #0. Obtener/actualizar datos de la OC por id
      Order.get(order_id)
      
      #0.1. Obtener/actualizar datos de los almacenes
      Almacen.get_almacenes
      
      order = Order.where(:order_id => order_id).first
      
      #VALIDACIÓN DE EXISTENCIA DE ORDEN DE COMPRA
      if order.present?
        
        #VALIDACION DE SKU
        if order.sku != '2' and order.sku != '3' and order.sku != '11' and order.sku != '40' and order.sku != '41'
          p "NO SE VENDE EL SKU ESPECIFICADO EN LA ORDEN DE COMPRA"
          p 'Order rejected'
          aux = Order.reject(order_id, "motivo: No se vende el sku especificad en la orden de compra")
          ApiConnection.reject_order(order_id, order.cliente)
          return false
        end
        
        #VALIDACION DE PRECIO
        if order.precioUnitario < Price.get_price(order.sku, order.created_at)
          p "PRECIO DE LA ORDEN DE COMPRA INFERIOR"
          p 'Order rejected'
          aux = Order.reject(order_id, "motivo: Precio en orden de compra inferior al precio del producto")
          ApiConnection.reject_order(order_id, order.cliente)
          return false
        end
          
        if order.estado == "aceptada" #evita asignar items a una orden que ya fue aceptada
          return true
        end
        
        #VALIDACION DE STOCK
        #Primero actualizamos nuestras bases de datos segun el sku de la orden de compra
        Almacen.update_almacenes_stock(order.sku)
        #Luego revisamos los items que estan disponibles
        items_stock = Item.available.where(:sku => order.sku) #available son los que no han sido asignados a ninguna orden de compra
        p 'stock:' + items_stock.length.to_s
        
        if items_stock.length > order.cantidad
          # Aceptar la orden
          p 'Order accepted'
          aux = Order.receipt(order_id)
          order_accepted = true
          p order.cliente
          ApiConnection.accept_order(order_id, order.cliente)
        else
          #Rechazar la orden
          p "NO HAY STOCK SUFICIENTE"
          p 'Order rejected'
          aux = Order.reject(order_id, "motivo: No hay suficiente stock")
          ApiConnection.reject_order(order_id, order.cliente)
        end
      else
        respond_to do |format|
          format.json {render json: { error: "id de orden no encontrado" } }
        end
      end
      return order_accepted
  end
  
  #Método que se inicia en b2b controller cuando llega una orden de compra
  def self.start_b2b_process(order_id)
    p "Starting B2B proccess"
    if !validar_OC(order_id) #Si no se valida la OC el método retorna
      return
    end
    
    order = Order.where(:order_id => order_id).first
    
    #Asignación o reservación de los itemes a cierta orden de compra
    items_available = Item.available.where(sku: order.sku)[0..order.cantidad-1]
    
    items_available.each do |it|
      it.asociar_oc(order_id)
    end
    
    
    #Crear factura
    resp = Invoice.emitir(order_id)
    invoice_id = resp.parsed_response['_id']
     
     # =>Avisar que se creó factura al grupo cliente
    ApiConnection.send_invoice(invoice_id , order.cliente)
    
    #Aquí se termina el proceso ya que debemos esperar hasta que nos avisen que la factura fue pagada
    #Se llamará al metodo b2b_proccess2
    
  end
  
  def self.b2b_process2(invoice_id)
    # =>Consultar factura para verificar que fue pagada (ver estado de factura)
    resp = Invoice.obtener(invoice_id)
    if resp[0]['estado'] != 'pagado'
      p 'La factura no fue pagada'
      # Que hacer en ese caso?
      return
    end
    
    oc_id = resp[0]['oc']
    order = Order.where(:order_id => oc_id).first
    
    #DESPACHAR AL GRUPO CLIENTE
    
    #Buscamos el id de la bodega de nuestro cliete
    if order.cliente == '1' || order.cliente == 'grupo1'
      n_grupo = 1
    elsif order.cliente == '2' || order.cliente == 'grupo2'
      n_grupo = 2
    elsif order.cliente == '3' || order.cliente == 'grupo3'
      n_grupo = 3
    elsif order.cliente == '4' || order.cliente == 'grupo4'
      n_grupo = 4
    elsif order.cliente == '5' || order.cliente == 'grupo5'
      n_grupo = 5
    elsif order.cliente == '6' || order.cliente == 'grupo6'
      n_grupo = 6
    elsif order.cliente == '7' || order.cliente == 'grupo7'
      n_grupo = 7
    elsif order.cliente == '8' || order.cliente == 'grupo8'
      n_grupo = 8
    end
    almacen_id = Almacen.array_recepciones[n_grupo-1][:reception_id]
    p 'almacen_id:' + almacen_id.to_s
    
    items_a_despachar = Order.where(order_id: oc_id).first.items.where(in_warehouse: 1) #Items que debo despachar
    
    #Primero debo asegurarme que esten los item en la bodega de despacho
    p "cantidad:"+items_a_despachar.length.to_s
    items_a_despachar.each do |i|
      if i.almacen.tipo != 'despacho'
        id_despacho = Almacen.where(tipo: 'despacho').first.almacen_id
        Almacen.mover_stock(id_despacho, i.item_id)
      end
      p i.item_id.to_s
      Almacen.mover_stock_bodega(almacen_id,i.item_id)
    end
    Almacen.update_almacenes_stock(order.sku) #Para actualizar nuestras bases de datos
  end

  def reponer_stock(sku, cantidad)
    #Obtenemos los datos del stock de producto sku
  end

  def mover_stock    
  end
  
  def bodega_status

    Almacen.get_almacenes

    @origen = Almacen.where.not(usedSpace: 0)
    @destino = Almacen.where("\"totalSpace\" - \"usedSpace\" > 0")

    @recepcion = Almacen.where(tipo: 'recepcion').first
    @general = Almacen.where(tipo: 'general')
    @despacho = Almacen.where(tipo: 'despacho').first
    @pulmon = Almacen.where(tipo: 'pulmon').first

    @stock_r = Almacen.get_sku(@recepcion.almacen_id)
    @stock_d = Almacen.get_sku(@despacho.almacen_id)
    @stock_p = Almacen.get_sku(@pulmon.almacen_id)

    @all_stock = @stock_r.clone
    @all_stock.concat(@stock_d).concat(@stock_p)
    @general.each do |g|
      s_g = Almacen.get_sku(g.almacen_id)
      @all_stock.concat(s_g)
    end
  end
  
  def oc_externa
    if params[:grupo] == "1"
      @co = ApiConsumerG1.create_order(params[:id])
    end
    if params[:grupo] == "2"
    
    end
    if params[:grupo] == "3"
      @co = ApiConsumerG3.create_order(params[:id])
    end
  end

  def bodega_status_refresh

    sku = params[:sku]
    cantidad = params[:cantidad]

    @items = Almacen.get_stock(params[:origen], params[:sku])

    for i in 0..(cantidad.to_i-1)
      Almacen.mover_stock(params[:destino], @items[i]["_id"])
    end

    redirect_to bodega_status_path

  end


  def despejar_recepcion
    
    #actualizo información de los almacenes
    Almacen.get_almacenes

    recepcion = Almacen.where(tipo: "recepcion").first
    general = Almacen.where(tipo: "general")
    despacho = Almacen.where(tipo: "despacho").first

    if recepcion.usedSpace.to_f/recepcion.totalSpace.to_f >= 0
      #Hay espacio en bodega(s) libre(s)?
      general.each do |g|
        general_libre = g.totalSpace - g.usedSpace
        inventario = Almacen.get_sku(recepcion.almacen_id)
        while general_libre > 0 and inventario.present?
          inventario = Almacen.get_sku(recepcion.almacen_id)
          inventario.sort_by{ |a| a["total"] }

          inventario.each do |i|
            if i["total"] <= general_libre
              #podemos moverlos todos
              stock = Almacen.get_stock(recepcion.almacen_id, i["_id"])
              stock.each do |s|
                Almacen.mover_stock(g.almacen_id, s["_id"])
                im = ItemMovement.new(date: DateTime.now, 
                  origen: recepcion.almacen_id, destino: g.almacen_id)
                im.save
              end
            else
              #movemos de a uno
              max = i["total"].to_i - general_libre
              stock = Almacen.get_stock(recepcion.almacen_id, i["_id"])
              for j in 0..(max - 1)
                Almacen.mover_stock(g.almacen_id, stock[j]["_id"])
                #, item_id: s["_id"]
                im = ItemMovement.new(date: DateTime.now, 
                  origen: recepcion.almacen_id, destino: g.almacen_id)
                im.save
              end
            end
            general_libre = g.totalSpace - g.usedSpace
          end
        end
      end
    end
    Almacen.get_almacenes

    recepcion = Almacen.where(tipo: "recepcion").first
    general = Almacen.where(tipo: "general")
    despacho = Almacen.where(tipo: "despacho").first
    
    respond_to do |format|
      format.html {redirect_to :bodega_status}
      format.json { render json: { recepcion: { totalSpace: recepcion.totalSpace, 
        usedSpace: recepcion.usedSpace } } }
    end
  end

  def fabrica
    @pedido = Fabrica.all
  end
  
  def producir
    @fabrica = Fabrica.new()
    @saldo = Bank.saldo(Bank.cuenta(7)).parsed_response['saldo']
  end
  def producir_send
    if Fabrica.producir(params[:sku], params[:cantidad]) == "error"
      redirect_to({ action: 'fabrica'}, alert: "Ese sku no es fabricado por nosotros" )
    else
      redirect_to :fabrica
    end
  end
  # Método para testear conexiones y saber como usar los métodos y procesar las respuestas
  def test_connection_with_systems
    #Debemos probar todos los sistemas
    #Definir parametros
    sku = 4
    cantidad = 5
    precio_unitario = 1732 
    fecha_entrega = DateTime.current() + 6.days
    canal = 'b2b'
    notas = 13
    proveedor = 'grupo2'
    #Crear oc
    @create_order = Order.create_order(sku, cantidad, precio_unitario, fecha_entrega, proveedor, canal, notas)
    #Obtener oc
    @get_order = Order.get(@create_order["_id"])
    # Como leer los parametros de get OC
    #recepcionar
    @accept_order = Order.receipt(@get_order[0]["_id"])
    #crear factura
    @emitir_invoice = Invoice.emitir(@accept_order.parsed_response[0]["_id"])
    #obtener factura
    @get_invoice = Invoice.obtener(@emitir_invoice.parsed_response["_id"])
      #Atributos de get invoice
      # -> @getinvoice.parsed_response[0]["x"]
      # donde x puede ser: _id , created_at , updated_at , cliente , proveedor ,
      #                    bruto , iva , total , oc , estado , _v
      # obtener cuenta otro grupo
    destino = '556489daefb3d7030091bab5' #Esta es la cuenta del grupo 2
    # Transferir
    monto = @get_invoice.parsed_response[0]["total"]
    # En monto debe ir el monto que se paga,
    # en este caso solo transferiremos un peso
    @transaction = Transaction.crear(monto-monto +1, ENV["KEY_BANCO"], destino)
    unless @transaction.parsed_response["Excepcion"]
    # Obtener transaccion
      @get_transaction = Transaction.obtener(@transaction.parsed_response["id"]) 
    else
      @transaction.parsed_response["Excepción"]
    end
    # pagar factura
    @pagar_factura = Invoice.pagar(@get_invoice.parsed_response[0]["_id"])
    # obtener cartola
    fecha_inicio = DateTime.current() - 10.days
    fecha_termino = DateTime.current()
    limit = 10
    # limit = 0 en caso de que se quiera dejar el default de transacciones (100)
    @cartola = Bank.cartola(fecha_inicio, fecha_termino, ENV["KEY_BANCO"], limit)
    # obtener saldo
    @saldo = Bank.saldo(ENV["KEY_BANCO"]) 
    # obtener transaccion 
    
    motivo = "Rechazo de prueba"
    # crear oc
    @create_order2 = Order.create_order(sku, cantidad, precio_unitario, fecha_entrega, proveedor, canal, notas)
    # crear factura
    @emitir_invoice2 = Invoice.emitir(@create_order2.parsed_response["_id"])
    # rechazar factura
    @invoice2 =Invoice.rechazar(@emitir_invoice2.parsed_response["_id"], motivo)
    # rechazar
    @order2 = Order.reject(@create_order2.parsed_response["_id"], motivo)

    motivo = "Anulacion de prueba"
    # crear oc
    @create_order3 = Order.create_order(sku, cantidad, precio_unitario, fecha_entrega, proveedor, canal, notas)
    # crear factura
    @emitir_invoice3 = Invoice.emitir(@create_order3.parsed_response["_id"])
    # anular factura
    @invoice3 = Invoice.anular(@emitir_invoice3.parsed_response["_id"], motivo)
    # anular
    @order3 = Order.cancel(@create_order3.parsed_response["_id"], motivo)   
  end
  #este ḿétodo se usa para entregar productos de un bodega a otra, recuerda que hay que transferir de una cosa
  def ship
  end

  def informacion_empresa
    @phone = "(61)(2) 6286 2098"
    @address = "10 Culgoa CircuitO'Malley, australia"
    @name = "Grupo7"
    @id = 7
    @country = "Australia"
    @continent= "OCEANIA"
    @bank_account_id = ENV["KEY_BANCO"]
  end


  #######################ACCIONES DASHBOARD##################

  def dashboards
  end

  def calidad_servicio
  end

  def cantidad_ventas
  end

  def costos
  end

  def montos_facturados
  end

  def reportes

    cuenta_propia = Bank.cuenta_grupo.select { |h| h[:group] == 7 }.first[:bank_account]
    @array_datos = Array.new
    @array_meses = Array.new

    inicio = Date.today.last_year
    datos = Bank.cartola(inicio, Date.today, cuenta_propia, 1000).parsed_response
    @ingresos = datos.select { |s| s["destino"] == cuenta_propia }

    for i in 0..11
      total = 0
      aux = @ingresos.select { |j| j["created_at"].to_date >= inicio.beginning_of_month && j["created_at"].to_date <= inicio.end_of_month }

      aux.each do |a|
        total += a["monto"].to_i
      end

      @array_datos << total
      @array_meses << inicio.strftime('%B')[0..2]
      inicio = inicio.next_month

    end

    @sku_data = Hash.new

    ["2","3","11","40","41"].each do |k|
      inicio = Date.today.last_year
      aux_array = Array.new

      for i in 0..3
        total = 0
        Order.where(sku: k).where(proveedor: "7").where(created_at: inicio.beginning_of_quarter..inicio.end_of_quarter).each do |s|
          total += s.cantidad.to_i * s.precioUnitario.to_i
        end
        aux_array << total
        inicio = inicio.next_quarter
      end
      @sku_data[k] = aux_array
    end


  end

end
