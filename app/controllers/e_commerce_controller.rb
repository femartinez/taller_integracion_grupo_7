class ECommerceController < ApplicationController

  before_action :authenticate_user!, except: [:index, :work, :contact, :us]

  def index
  end

  def list_products
    @products = Sku.all
  end

  # El formulario para agregar productos debe llamar a esde metodo
  # en los parametros se deben incluir: 
  #  - sku_id
  #  - quantity
  def add_product_to_cart

    if params[:quantity].to_i > 0

      sc = ShopCart.where(user_id: current_user.id.to_i).first
      sc = ShopCart.create(user_id: current_user.id.to_i) unless sc
      li = LineItem.new(sku_id: params[:sku_id], quantity: params[:quantity], unit_price: Price.get_price(params[:sku_id], DateTime.now), status: 0)
      li.shop_cart = sc
      li.save
      p sc.line_items.each

      redirect_to e_commerce_show_cart_path
    else
      redirect_to e_commerce_path
    end
  end

  # Recibe como parametro el id del line item que debe eliminarse
  # del carro de compras
  #  - li_id
  def delete_product_from_cart
    LineItem.find(params[:li_id].to_i).destroy
    redirect_to controller: 'e_commerce', action: 'show_cart'
  end

  def show_cart

    @cart = ShopCart.where(user_id: current_user.id).first
    @cart = ShopCart.create(user_id: current_user.id) unless @cart

    @line_items = @cart.line_items.pending
    @total = 0

    @line_items.each do |l|
      @total += Price.get_price(l.sku_id, DateTime.now).to_i * l.quantity.to_i
    end

  end

  def confirm_order
  end

  def pay
    @cart = ShopCart.where(user_id: current_user.id).first
    if @cart.calcular_total > 0
      response = Invoice.crear_boleta(current_user.id, @cart.calcular_total)
      p params[:dir]
      boleta_id = response.parsed_response['_id']  if response.code == 200
      redirect_to ECommerce.link_web_pay(@cart.id, boleta_id, params[:dir])
    end
  end

  def pay_success
    @cart = ShopCart.find(params[:cart_id]).vaciar
    redirect_to e_commerce_path, notice: 'Compra realizada con exito, despacho en proceso'
  end

  def pay_canceled
    redirect_to e_commerce_path, notice: 'Compra cancelada'
  end

  def ship
  end

  def work
  end

  def contact
  end

  def us
  end
end
