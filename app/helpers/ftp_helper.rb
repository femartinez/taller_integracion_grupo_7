module FtpHelper

  def ocs
    [
      ['ID', 'oc'],
      ['Cliente', 'client'],
      ['Proveedor', 'proveedor'],
      ['SKU', 'sku'],
      ['Fecha de Entrega', 'date'],
      ['Cantidad', 'quantity'],
      ['Precio Unitario', 'unit_price'],
    ]
  end	
end
