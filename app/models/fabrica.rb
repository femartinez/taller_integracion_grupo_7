class Fabrica < ActiveRecord::Base
require 'hmac-sha1'
  def self.producir(sku, cantidad)
    # calcular cantidad real a producir (por lotes)
    cantidad_produccion = cantidad_a_producir(sku, cantidad)
    return cantidad_produccion if cantidad_produccion == 'error'
    # Validar stock necesario en bodega de insumos para producir sku
    if Almacen.validar_stock_insumos_despacho(sku, cantidad_produccion)
      # si hay insumos, entonces debemos hacer la trx y fabricar
      costo_unitario_produccion = costo_produccion_sku(sku)
      cuenta = get_cuenta_fabrica
      monto = cantidad_produccion * costo_unitario_produccion
      Bank.set_cuenta_banco
      trx = Transaction.crear(monto, @bank_account_id, cuenta)
      create_request(sku, cantidad_produccion, trx)
      if @orden_fabricacion['_id']
        f = Fabrica.new(sku: @orden_fabricacion['sku'], cantidad: @orden_fabricacion['cantidad'], fecha_despacho: @orden_fabricacion['disponible'], status: 'pendiente')
        f.save
      end
      return true
    else
      # Significa que no hay suficiente stock de insumos
      Fabrica.array_formula_lote.each do |f|
        if f[:sku] == sku
          cantidad_necesaria = f[:cantidad_insumo].to_i
          precio = Price.get_price(sku, DateTime.now)
          datos_sku = Fabrica.array_productores.select { |x| x[:sku] == sku }.first 
          o = Order.create_order(sku, cantidad_necesaria, precio, (Date.today + 2.days), datos_sku[:productor], 'b2b', 'asap')
          ApiConnection.create_order(o.parsed_response['_id'], o.parsed_response['proveedor'])
        end
      end
    end

    return false
  end

  def self.get_cuenta_fabrica
    set_host
    uri = URI.parse(@host + '/fabrica/getCuenta')
    # Creamos el hash y header
    create_hash_sha1('GET')
    getCuenta = HTTParty.get(uri,:headers => @header)
    return getCuenta.parsed_response['cuentaId']
  end

  def self.create_request(sku, cantidad, trx_response)
    set_host
    uri = URI.parse(@host + '/fabrica/fabricar')
    create_hash_sha1('PUT' + sku.to_s + cantidad.to_s + trx_response.parsed_response['id'])
    body = { :sku => sku, :trxId => trx_response.parsed_response['id'] ,:cantidad => cantidad}
    @orden_fabricacion = HTTParty.put(uri,:body => body.to_json,:headers => @header).parsed_response
  end

  def self.reponer_stock
    min_lev = min_levels
    #Para cada sku ver cuanto tenemos
  end

  private

  def self.create_hash_sha1(string_1)
    @hash = Base64.encode64((HMAC::SHA1.new("M0aD0pJlFNrASW") << string_1).digest).strip
    @header = { 'Authorization' => 'INTEGRACION grupo7:' + @hash, 'Content-Type' => 'application/json' }
  end

  def self.set_host
    @host = ''
    if Rails.env == 'development'
      @host = ENV['HOST_BODEGA']
    else
      @host = "http://integracion-2015-prod.herokuapp.com/bodega"
    end
  end

  def self.costo_produccion_sku(sku)
    array_costo
    index = @array_costo.index { |h| h[:sku] == sku.to_i }
    @array_costo[index][:costo_unitario]
  end

  def self.cantidad_a_producir(sku, cantidad)
    #devuelve el número de lotes a producir según la cantidad total de productos
    datos_sku = Fabrica.array_costo.select {|h| h[:sku] == sku.to_i }.first
    # A producir
    unless datos_sku.nil?
      cantidad_lote = datos_sku[:cantidad_lote]
      return cantidad_lote*(cantidad.to_f/cantidad_lote.to_f).ceil.to_i
    else
      return "error"
    end
  end

  def self.min_levels
    [
      { sku: 2, nombre: 'huevo', min_level: 700 },
      { sku: 3, nombre: 'maiz', min_level: 800 },
      { sku: 11, nombre: 'margarina', min_level: 300 },
      { sku: 40, nombre: 'queso', min_level: 400 },
      { sku: 41, nombre: 'suero de leche', min_level: 300 }
    ]
  end

  def self.array_costo
    @array_costo = [
        {sku: 2, nombre: 'huevo', tipo: 'raw material', grupo: 7, unidad: 'UN', costo_unitario: 1289, tiempo: 2.0, cantidad_lote: 1 },
        {sku: 3, nombre: 'maiz', tipo: 'raw material', grupo: 7, unidad: 'UN', costo_unitario: 1370, tiempo: 4.3, cantidad_lote: 1 },
        {sku: 11, nombre: 'margarina', tipo: 'finished goods', grupo: 7, unidad: 'UN', costo_unitario: 2003, tiempo: 1.5, cantidad_lote: 900 },
        {sku: 40, nombre: 'queso', tipo: 'finished goods', grupo: 7, unidad: 'UN', costo_unitario: 3299, tiempo: 1.5, cantidad_lote: 900 },
        {sku: 41, nombre: 'suero de leche', tipo: 'finished goods', grupo: 7, unidad: 'UN', costo_unitario: 29691, tiempo: 2.1, cantidad_lote: 200 }
      ]
  end

  def self.array_formula_lote
    @array_formula_lote = [
        {sku: 11, sku_insumo: 4, nombre_insumo: 'Aceite de Maravilla',cantidad_insumo: 828, unidad: 'UN'},
        {sku: 40, sku_insumo: 7, nombre_insumo: 'Leche',cantidad_insumo: 1000, unidad: 'UN'},
        {sku: 41, sku_insumo: 7, nombre_insumo: 'Leche',cantidad_insumo: 2000, unidad: 'UN'}
      ]
  end

  def self.array_productores
    @array_productores = [
        {sku: 4, productor: 1, precio: 1732, tiempo: 3.1 },
        {sku: 7, productor: 6, precio: 1696, tiempo: 2.0 }
      ]
  end
 


end
