class Transaction < ActiveRecord::Base
  require 'uri'
  require 'httparty'
  require 'json'
  require 'date'

  belongs_to :invoice
  # Crea una transaccion en el sistema de banco
  # PI: Ver la respuesta y comprobar que se haya
  # creado efectivamente, si es asi guardarla en la base de datos
  def self.crear(monto, origen, destino)
    set_host
    set_header
    uri = URI(@host + '/transferir')
    query = { monto: monto, origen: origen, destino: destino }
    @response = HTTParty.post(uri, query: query, headers: @header)
    if @response.code == 200
      r = @response.parsed_response
      # En caso de que se cree debemos crear la transaccion en nuestro sistema
      t = Transaction.new(transaction_id: r['id'], amount: r['monto'],
                          date: r['created_at'], emisor: r['origen'],
                          receptor: r['destino']
                         )
      t.save
    end
    @response
  end

  # Obtiene una transaccion especifica
  # PI: Hacer update o crear la transaccion en la base de datos
  def self.obtener(transaction_id)
    set_host
    set_header
    uri = URI(@host + '/transaccion/' + transaction_id)
    @response = HTTParty.get(uri, headers: @header)
  end

  private

  def self.set_host
    if Rails.env == 'development'
      @host = ENV['HOST_BANCO']
    else
      @host = 'http://moyas.ing.puc.cl:8080/Integra7V2/webresources/banco'
    end
  end

  def self.set_header
    @header = { 'Content-Type' => 'application/json' }
  end

  def self.generic_response
    respond_to do |format|
      format.json { render json: @response.body, status: @response.code }
    end
  end


end
