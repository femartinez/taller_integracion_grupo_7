class Item < ActiveRecord::Base

  belongs_to :order
  belongs_to :almacen
  has_many :item_movements

  # Productos de venta
  scope :output, -> { where(tipo: 'output') }
  # Insumos
  scope :input, -> { where(tipo: 'input') }

  scope :available, -> { where(reserved: 0) }
  scope :reserved, -> { where(reserved: 1) }

  def self.save_items(data)

    data.each do |d|
      i = Item.where(:item_id => d["_id"])
      if i.first.nil?
        # tengo que crear el almacen en la bd
        i_nuevo = Item.new(item_id: d["_id"], almacen_id: d["almacen"], sku: d["sku"])
        i_nuevo.save
        im_nuevo = ItemMovement.new(date: DateTime.now, item_id: d["_id"], origen: d["almacen"], destino: d["almacen"])
        im_nuevo.save
      else
        # actualizo los atributos del almacen para evitar inconsistencias
        i.first.assign_attributes(almacen_id: d["almacen"], sku: d["sku"])
        i.first.save
        #i_lastmovement = ItemMovement.where(item_id: i.first.id).last.destino
        #im_nuevo = ItemMovement.new(date: DateTime.now, item_id: i.first.id, origen: i_lastmovement, destino: d["almacen"])
        #im_nuevo.save
      end
    end
  end

  def despachar_producto

  end

  def mover(almacen)
    item_movement.new
  end

  def asociar_oc(order_id)
    o = Order.where(:order_id => order_id).first
    unless o
      Order.get(order_id)
      o = Order.where(:order_id => order_id).first
    end
    if o && reserved == 0
      self.order = o
      self.reserved = 1
      self.save
      true
    else
      false
    end
  end

end
