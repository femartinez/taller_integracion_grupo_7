class Grupo5 < ActiveRecord::Base

    require 'httparty'
    require 'uri'

    def self.create_group
        set_host
        uri = URI(@host+"/b2b/new_user")
        header = {'Content-Type' =>'application/json', 'Accept' => 'application/json'}
        query = {'username' => 'tallergrupo7' , 'password' =>'tallergrupo7'}
        resp = HTTParty.put(uri,:query => query, :header => header )
        response = JSON.parse(resp.body)
        return response
    end

    def self.get_token
        set_host
        uri = URI(@host+"/b2b/get_token")
        header = {'Content-Type' =>'application/json', 'Accept' => 'application/json'}
        query = {"username" => "tallergrupo7" , "password" => "tallergrupo7"}
        resp = HTTParty.post(uri, :query => query, :header => header )
        @response = JSON.parse(resp.body)
        return @response['token']
    end

    def self.create_order(order_id)
        set_host
        token = get_token 
        header = { 'Authorization' => 'Token '+token, 'Content-Type' =>'application/json', 'Accept' => 'application/json'}
        uri = URI(@host+"/b2b/new_order")
        query = {"order_id" => order_id }
        resp = HTTParty.post(uri,:headers => header, :query => query)
        @response = JSON.parse(resp.body)
        return @response
    end

    def self.accept_order(order_id)
        set_host
        token = get_token
        uri = URI(@host+"/b2b/order_accepted")
        header = { 'Authorization' => 'Token '+token, 'Content-Type' =>'application/json', 'Accept' => 'application/json'}
        query = {"order_id" => order_id}
        resp = HTTParty.post(uri,:headers => header, :query => query)
        @response = JSON.parse(resp.body)
        return @response
    end

    def self.cancel_order(order_id)
        set_host
        token = get_token
        uri = URI(@host+"/b2b/order_canceled")
        header = { 'Authorization' => 'Token '+token, 'Content-Type' =>'application/json', 'Accept' => 'application/json'}
        query = {"order_id" => order_id}
        resp = HTTParty.post(uri,:headers => header, :query => query)
        @response = JSON.parse(resp.body)
        return @response
    end

    def self.reject_order(order_id)
        set_host
        token = get_token
        uri = URI(@host+"/b2b/order_rejected")
        header = { 'Authorization' => 'Token '+token, 'Content-Type' =>'application/json', 'Accept' => 'application/json'}
        query = {"order_id" => order_id}
        resp = HTTParty.post(uri,:headers => header, :query => query)
        @response = JSON.parse(resp.body)
        return @response
    end

    def self.send_invoice(invoice_id)
        set_host
        token = get_token
        uri = URI(@host+"/b2b/invoice_created")
        header = { 'Authorization' => 'Token '+token, 'Content-Type' =>'application/json', 'Accept' => 'application/json'}
        query = {"invoice_id" => invoice_id}
        resp = HTTParty.post(uri,:headers => header, :query => query)
        @response = JSON.parse(resp.body)
        return @response
    end

    def self.invoice_paid(invoice_id)
        set_host
        token = get_token
        uri = URI(@host+"/b2b/invoice_paid")
        header = { 'Authorization' => 'Token '+token, 'Content-Type' =>'application/json', 'Accept' => 'application/json'}
        query = {"invoice_id" => invoice_id}
        resp = HTTParty.post(uri,:headers => header, :query => query)
        @response = JSON.parse(resp.body)
        return @response
    end
    
    def self.reject_invoice(invoice_id)
        set_host
        token = get_token
        uri = URI(@host+"/b2b/invoice_rejected")
        header = { 'Authorization' => 'Token '+token, 'Content-Type' =>'application/json', 'Accept' => 'application/json'}
        query = {"invoice_id" => invoice_id}
        resp = HTTParty.post(uri,:headers => header, :query => query)
        @response = JSON.parse(resp.body)
        return @response
    end


    private
    
    def self.set_host
        @host = 'http://integra5.ing.puc.cl'
    end

    def self.set_header_auth
        @header = {'Content-Type' =>'application/json', 'Accept' => 'application/json', 'Authorization' => 'Token '+self.get_token}
    end
end
