class Grupo8 < ActiveRecord::Base

	require 'httparty'
	require 'uri'

	def self.create_group
        set_host
        uri = URI(@host+"/b2b/new_user")
        header = {'Content-Type' =>'application/json', 'Accept' => 'application/json'}
        body = {'username' => 'grupo7' , 'password' =>'grupo7'}
        resp = HTTParty.put(uri,:body => body.to_json, :header => header )
        response = JSON.parse(resp.body)
        return response
    end

	def self.get_token
        self.set_host
        header = {'Content-Type' =>'application/json', 'Accept' => 'application/json'}
        uri = URI(@host+"/b2b/get_token")
        body = {"username" => "grupo7" , "password" => "grupo7"}
        resp = HTTParty.post(uri, :body => body.to_json)
        @response = JSON.parse(resp.body)
		return @response['token']
    end

    def self.create_order(order_id, bodega_id)
        self.set_host
        token = get_token
        self.set_header_auth
        header = {'Content-Type' =>'application/json', 'Accept' => 'application/json', 'Authorization' => 'Token '+token}
        uri = URI(@host+"/b2b/new_order")
        body = {"order_id" => order_id , "bodega_id" => bodega_id}
        resp = HTTParty.post(uri, :body => body.to_json, :headers => @header)
        @response = JSON.parse(resp.body)
		return @response
    end

	#Para cuando aceptamos una orden de compra del grupo 6
    def self.accept_order(order_id)
        set_host
        set_header_auth
        uri = URI(@host+"/b2b/order_accepted")
        body = {"order_id" => order_id}
        resp = HTTParty.post(uri, :body => body.to_json, :headers => @header)
        @response = JSON.parse(resp.body)
        return @response
    end

    def self.cancel_order(order_id)
        set_host
        token = get_token
        uri = URI(@host+"/b2b/order_canceled")
        header = { 'Authorization' => 'Token '+token, 'Content-Type' =>'application/json', 'Accept' => 'application/json'}
        query = {"order_id" => order_id}
        resp = HTTParty.post(uri,:headers => header, :query => query)
        @response = JSON.parse(resp.body)
        return @response
    end

    def self.reject_order(order_id)
        ##Tira error
        set_host
        token = get_token
        uri = URI(@host+"/b2b/order_rejected")
        header = { 'Authorization' => 'Token '+token, 'Content-Type' =>'application/json', 'Accept' => 'application/json'}
        query = {"order_id" => order_id}
        resp = HTTParty.post(uri,:headers => header, :body => query.to_json)
        @response = JSON.parse(resp.body)
        return @response
    end

    def self.send_invoice(invoice_id)
        set_host
        token = get_token
        uri = URI(@host+"/b2b/invoice_created")
        header = { 'Authorization' => 'Token '+token, 'Content-Type' =>'application/json', 'Accept' => 'application/json'}
        query = {"invoice_id" => invoice_id}
        resp = HTTParty.post(uri,:headers => header, :query => query)
        @response = JSON.parse(resp.body)
        return @response
    end

    def self.invoice_paid(invoice_id)
        self.set_host
        self.set_header_auth
        uri = URI(@host+"/b2b/invoice_paid")
        token = get_token
        body = {"invoice_id" => invoice_id}
        resp = HTTParty.post(uri, :body => body.to_json, :headers => @header)
        @response = JSON.parse(resp.body)
        return @response
    end

    def self.reject_invoice(invoice_id)
        set_host
        set_header_auth
        uri = URI(@host+"/b2b/invoice_rejected")
        body = {"invoice_id" => invoice_id}
        resp = HTTParty.post(uri, :body => body.to_json, :headers => @header)
        @response = JSON.parse(resp.body)
        return @response
    end

    def self.get_bank_account
    	set_host
    	set_header_auth
    	uri = URI(@host+"/b2b/bank_account")
    	resp = HTTParty.get(uri)
    	@response = JSON.parse(resp.body)
    	return @response
    	
    end

	private
	def self.set_host
		@host = 'http://integra8.ing.puc.cl'
	end
	def self.set_header
		@header = {'Content-Type' =>'application/json', 'Accept' => 'application/json'}
	end

    def self.set_header_auth
        @header = {'Content-Type' =>'application/json', 'Accept' => 'application/json', 'Authorization' => 'Token '+self.get_token}
    end

	

end
