class Invoice < ActiveRecord::Base
  belongs_to :order

  # Metodo para crear o actualizar una factura en el sistema
  # Params: r --> parsed_response
  def self.save_or_update(r)
    if Invoice.where(invoice_id: r['_id']).first
      i = Invoice.where(invoice_id: r['_id']).first
    else
      i = Invoice.new
    end
    i.invoice_id = r['_id']
    i.creation_date = r['created_at']
    i.supplier = r['proveedor']
    i.client = r['cliente']
    i.net_amount = r['bruto']
    i.tax = r['iva']
    i.total_value = r['total']
    i.payment_status = r['estado']
    order = Order.where(order_id: r['oc']).first
    unless order
      Order.get(r['oc'])
      order = Order.where(order_id: r['oc']).first
    end
    i.order = order
    # Si la factura fue pagada seteamos la fecha de pago
    i.payment_date = r['updated_at'] if r['estado'] == 'pagada'
    # Si fue rechazada seteamos el motivo de rechazo
    i.reject = r['rechazo'] if r['estado'] == 'rechazada'
    # Si fue anulada seteamos el motivo de anulacion
    i.cancel = r['anulacion'] if r['estado'] == 'anulada'
    i.save
  end

  # Descripcion: Metodo que permite emitir una factura a partir de
  # una orden de compra
  def self.emitir(oc_id)
    set_host
    set_header
    uri = URI.parse(@host + '/crear/' + oc_id)
    @response = HTTParty.put(uri, headers: @header)
    save_or_update(@response.parsed_response) if @response.code == 200
    @response
  end

  # Descripcion: Metodo que permite a un proveedor obtener una factura
  def self.obtener(factura_id)
    set_host
    set_header
    uri = URI.parse(@host + '/obtener/' + factura_id)
    @response = HTTParty.get(uri)
    save_or_update(@response.parsed_response[0]) if @response.code == 200
    @response
  end

  # Descripcion: Metodo que permite a un proveedor marcar una factura
  # como pagada
  def self.pagar(factura_id)
    set_host
    set_header
    uri = URI.parse(@host + '/pay/' + factura_id)
    @response = HTTParty.post(uri, headers: @header)
    save_or_update(@response.parsed_response[0]) if @response.code == 200
    @response
  end

  # Descripcion: Metodo que permite a un cliente rechazar una factura
  # creada por un proveedor
  def self.rechazar(factura_id, motivo)
    set_host
    set_header
    uri = URI.parse(@host + '/rechazar/' + factura_id)
    query = { motivo: motivo }
    @response = HTTParty.post(uri, query: query, headers: @header)
    save_or_update(@response.parsed_response[0]) if @response.code == 200
    @response
  end

  # Descripcion: Metodo que permite a un proveedor anular la factura
  # ya emitida por nosotros
  def self.anular(factura_id, motivo)
    set_host
    set_header
    uri = URI.parse(@host + '/cancelar/' + factura_id)
    query = { motivo: motivo }
    @response = HTTParty.post(uri, query: query, headers: @header)
    save_or_update(@response.parsed_response[0]) if @response.code == 200
    @response
  end
  # Metodo que permite crear boletas, se usa para el sistema B2C
  # monto_total -> entero o float
  # cliente_id -> string
  def self.crear_boleta(cliente_id, monto_total)
    @host = set_host
    set_header
    set_id_grupo
    uri = URI.parse('http://moyas.ing.puc.cl:8080/Integra7V2/webresources/facturas' + '/boleta')
    query = { proveedor_id: @group_id, cliente_id: cliente_id, total: monto_total.ceil.to_i.to_s }
    @response = HTTParty.put(uri, query: query, headers: @header)
  end

  private

  def self.set_host
    if Rails.env == 'development'
      @host = ENV['HOST_SII']
    else
      @host = 'http://moyas.ing.puc.cl:8080/Integra7V2/webresources/facturas'
    end
  end

  def self.set_id_grupo
    if Rails.env == 'development'
      @group_id = '556489daefb3d7030091bab0'
    else
      @group_id = '55648ad2f89fed0300524ffb'
    end
  end
  
  def self.set_header
    @header = { 'Content-Type' => 'application/json' }
  end
end
