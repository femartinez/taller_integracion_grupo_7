class Ftp < ActiveRecord::Base

  require 'net/sftp'

  def self.connect
    set_ftp
    Net::SFTP.start(@server, @user, :password => @password) do |sftp|
      #Ingresamos a la carpeta de Pedidos en el FTP
      sftp.dir.foreach("/Pedidos") do |entry|
        #Guardamos el nombre de cada uno
        fileName = entry.name
        if fileName!= "." && fileName != ".."
          #Lo guardamos localmente
          sftp.download!("/Pedidos/" + fileName, @path + fileName)
          puts fileName
        end
      end
    end
  end

  def self.set_ftp
    if Rails.env == 'development'
      @server = 'chiri.ing.puc.cl'
      @user = 'integra7'
      @password = 'M2yW.3*Hc'
      @path = Rails.root.to_s + '/shared/tmp/pedidos/'
    else
      @server = 'moyas.ing.puc.cl'
      @user = 'integra7'
      @password = 'M2yW.3*Hc'
      @path = '/home/administrator/apps/taller_integracion_grupo_7/shared/tmp/pedidos/'
    end
  end

end
