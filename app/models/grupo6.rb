class Grupo6 < ActiveRecord::Base

	require 'httparty'
	require 'uri'

    def self.test_whenever

        Rails.logger.info("caca")

    end
	def self.create_group
        set_host
        uri = URI(@host+"/b2b/new_user")
        query = {"username" => "grupo7" , "password" => "grupo7"}
        set_header
        resp = HTTParty.post(uri, :query => query, :headers => @header)
        @response = JSON.parse(resp.body)
        token = @response['token']
        if token == nil
            p @response['message']
            token = get_token
        else
            token = @response['token']
        end
        return token
    end

	def self.get_token

        set_host
        uri = URI(@host+"/b2b/get_token/")
        header = {'Content-Type' =>'application/json', 'Accept' => 'application/json'}
        set_header_auth
        body = {'username' => "grupo7" , 'password' =>"grupo7"}
        resp = HTTParty.post(uri,:body => body, :headers => header )
        @response = JSON.parse(resp.body)
        return @response['token']

    end



    def self.create_order(order_id)
        self.set_host
        header = { "Authorization Token" => self.get_token}
        uri = URI(@host+"/b2b/new_order")
        body = {"order_id" => order_id}
        resp = HTTParty.post(uri, :body => body, :headers => header)

		return resp
    end

	#Para cuando aceptamos una orden de compra del grupo 6
    def self.accept_order(order_id)
        set_host
        set_header_auth
        uri = URI(@host+"/b2b/order_accepted")
        query = {"order_id" => order_id}
        resp = HTTParty.post(uri, :query => query, :headers => @header)
        @response = JSON.parse(resp.body)
        return @response
    end

    def self.cancel_order(order_id)
        self.set_host
        self.set_header_auth
        uri = URI(@host+"/b2b/order_canceled")
        token = get_token
        query = {"order_id" => order_id}
        resp = HTTParty.post(uri, :query => query, :headers => @header)
        @response = JSON.parse(resp.body)
        return @response
    end

    def self.reject_order(order_id)
        self.set_host
        self.set_header_auth
        uri = URI(@host+"/b2b/order_rejected")
        token = get_token
        query = {"order_id" => order_id}
        resp = HTTParty.post(uri, :query => query, :headers => @header)
        @response = JSON.parse(resp.body)
        return @response
    end

    def self.send_invoice(invoice_id)
        self.set_host
        self.set_header_auth
        uri = URI(@host+"/b2b/invoice_created")
        query = {"invoice_id" => invoice_id}
        resp = HTTParty.post(uri, :query => query, :headers => @header)
        @response = JSON.parse(resp.body)
        return @response
    end

    def self.invoice_paid(invoice_id)
        self.set_host
        self.set_header_auth
        uri = URI(@host+"/b2b/invoice_paid")
        token = get_token
        query = {"invoice_id" => invoice_id}
        resp = HTTParty.post(uri, :query => query, :headers => @header)
        @response = JSON.parse(resp.body)
        return @response
    end

    def self.reject_invoice(invoice_id)
        set_host
        set_header_auth
        uri = URI(@host+"/b2b/invoice_rejected")
        query = {"invoice_id" => invoice_id}
        resp = HTTParty.post(uri, :query => query, :headers => @header)
        @response = JSON.parse(resp.body)
        return @response
    end

	private
    
	def self.set_host
		@host = 'http://integra6.ing.puc.cl'
	end
	def self.set_header
		@header = {'Content-Type' =>'application/json', 'Accept' => 'application/json'}
	end

    def self.set_header_auth
        @header = {'Content-Type' =>'application/json', 'Accept' => 'application/json', 'Authorization' => 'Token '+self.get_token}
    end

	



end
