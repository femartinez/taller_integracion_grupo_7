class ECommerce < ActiveRecord::Base

  def self.link_web_pay(cart_id, boleta_id, dir)
    success_path = 'e_commerce_pay_success'
    cancel_path = 'e_commerce_pay_cancel'
    callback_base = ''
    callback_query = '?cart_id=' + cart_id.to_s + '&pedido_id=' + boleta_id + '&dir=' + dir

    if Rails.env == 'development'
      callback_base = 'http://localhost:3000/'
    else
      callback_base = 'http://integra7.ing.puc.cl/'
    end
    success_url = callback_base + success_path + callback_query
    cancel_url = callback_base + cancel_path + callback_query
    
    success_url =  URI.escape(success_url, Regexp.new("[^#{URI::PATTERN::UNRESERVED}]"))
    cancel_url =  URI.escape(cancel_url, Regexp.new("[^#{URI::PATTERN::UNRESERVED}]"))

    callback_success = 'callbackUrl=' + success_url
    callback_cancel = '&cancelUrl=' + cancel_url
    boleta = '&boletaId=' + boleta_id
    url_base = ''
    if Rails.env == 'development'
      url_base = 'http://chiri.ing.puc.cl/banco/pagoenlinea?'
    else
      url_base = 'http://moyas.ing.puc.cl/banco/pagoenlinea?'
    end
    url_base + callback_success + callback_cancel + boleta
  end


  def self.array_anual

    cuenta_propia = Bank.cuenta_grupo.select { |h| h[:group] == 7 }.first[:bank_account]
    array_datos = Array.new
    array_meses = Array.new

    inicio = Date.today.last_year
    datos = Bank.cartola(inicio, Date.today, cuenta_propia, 1000).parsed_response
    @ingresos = datos.select { |s| s["destino"] == cuenta_propia }

    for i in 0..11
      total = 0
      aux = @ingresos.select { |j| j["created_at"].to_date >= inicio.beginning_of_month && j["created_at"].to_date <= inicio.end_of_month }

      aux.each do |a|
        total += a["monto"].to_i
      end

      array_datos << total.to_i
      array_meses << inicio.strftime("%B")[0..2]
      inicio = inicio.next_month
    end
    array_datos
  end

end
