class ApiConnection < ActiveRecord::Base
#Creado por Diego
#Modelo para agrupar todos los modelos de api consumer
  
  def self.create_order(order_id, numero_grupo)
    num = get_numero_grupo(numero_grupo)
    if (num==1)
      ApiConsumerG1.create_order(order_id)
    elsif (num==2)
      ApiConsumerG2.create_order(order_id)
    elsif (num==3)
      ApiConsumerG3.create_order(order_id)
    elsif (num==5)
      Grupo5.create_order(order_id)
    elsif (num==6)
      Grupo6.create_order(order_id)
    elsif (num==7) #Para pruebas
      p 'Orden enviada satisfactoriamente'
    elsif (num==8)
      Grupo8.create_order(order_id)
    end
  end
  
  def self.accept_order(order_id,numero_grupo)
    num = get_numero_grupo(numero_grupo)
    if (num==1)
      ApiConsumerG1.accept_order(order_id)
    elsif (num==2)
      ApiConsumerG2.accept_order(order_id)
    elsif (num==3)
      ApiConsumerG3.accept_order(order_id)
    elsif (num==3)
      Grupo5.accept_order(order_id)
    elsif (num==6)
      Grupo6.accept_order(order_id)
    elsif (num == 7)#Para pruebas
      p 'Orden de prueba aceptada'
    elsif (num==8)
      Grupo8.accept_order(order_id)
    end
  end
  
  def self.cancel_order(order_id,numero_grupo)
    num = get_numero_grupo(numero_grupo)
    if (num==1)
      ApiConsumerG1.cancel_order(order_id)
    elsif (num==2)
      ApiConsumerG2.cancel_order(order_id)
    elsif (num==3)
      ApiConsumerG3.cancel_order(order_id)
    elsif (num==5)
      Grupo5.cancel_order(order_id)
    elsif (num==6)
      Grupo6.cancel_order(order_id)
    elsif (num==8)
      Grupo8.cancel_order(order_id)
    end
  end
  
  def self.reject_order(order_id,numero_grupo)
    num = get_numero_grupo(numero_grupo)
    if (num==1)
      ApiConsumerG1.reject_order(order_id)
    elsif (num==2)
      ApiConsumerG2.reject_order(order_id)
    elsif (num==3)
      ApiConsumerG3.reject_order(order_id)
    elsif (num == 7) #Para pruebas
      p 'Orden de prueba rechazada'
    elsif (num==5)
      Grupo5.reject_order(order_id)
    elsif (num==6)
      Grupo6.reject_order(order_id)
    elsif (num==8)
      Grupo8.reject_order(order_id)
    end
  end
  
  def self.send_invoice(invoice_id,numero_grupo)
    num = get_numero_grupo(numero_grupo)
    if (num==1)
      ApiConsumerG1.send_invoice(invoice_id)
    elsif (num==2)
      ApiConsumerG2.send_invoice(invoice_id)
    elsif (num==3)
      ApiConsumerG3.send_invoice(invoice_id)
    elsif (num==5)
      Grupo5.send_invoice(invoice_id)
    elsif (num==6)
      Grupo6.send_invoice(invoice_id)
    elsif (num == 7) #Para pruebas
      p 'Factura de prueba enviada'
    elsif (num==8)
      Grupo8.send_invoice(invoice_id)
    end
  end
  
  def self.reject_invoice(invoice_id,numero_grupo)
    num = get_numero_grupo(numero_grupo)
    if (num==1)
      ApiConsumerG1.reject_invoice(invoice_id)
    elsif (num==2)
      ApiConsumerG2.reject_invoice(invoice_id)
    elsif (num==3)
      ApiConsumerG3.reject_invoice(invoice_id)
    elsif (num==5)
      Grupo5.reject_invoice(invoice_id)
    elsif (num==6)
      Grupo6.reject_invoice(invoice_id)
    elsif (num==8)
      Grupo8.reject_invoice(invoice_id)
    end
  end
  
  def self.invoice_paid(invoice_id,numero_grupo)
    num = get_numero_grupo(numero_grupo)
    if (num==1)
      ApiConsumerG1.invoice_paid(invoice_id)
    elsif (num==2)
      ApiConsumerG2.invoice_paid(invoice_id)
    elsif (num==3)
      ApiConsumerG3.invoice_paid(invoice_id)
    elsif (num==5)
      Grupo5.invoice_paid(invoice_id)
    elsif (num==6)
      Grupo6.invoice_paid(invoice_id)
    elsif (num==8)
      Grupo8.invoice_paid(invoice_id)
    end
  end
  
  #Método para convertir el string que puede venir de dos maneras en el int que necesitan nuestros métodos.
  def self.get_numero_grupo(numero_grupo)
    if (numero_grupo == '1' || numero_grupo=='grupo1')
      return 1
    elsif (numero_grupo == '2' || numero_grupo=='grupo2')
      return 2
    elsif (numero_grupo == '3' || numero_grupo=='grupo3')
      return 3
    elsif (numero_grupo == '4' || numero_grupo=='grupo4')
      return 4
    elsif (numero_grupo == '5' || numero_grupo=='grupo5')
      return 5
    elsif (numero_grupo == '6' || numero_grupo=='grupo6')
      return 6
    elsif (numero_grupo == '7' || numero_grupo=='grupo7')
      return 7
    elsif (numero_grupo == '8' || numero_grupo=='grupo8')
      return 8
      
    end
        
  end
  
end
