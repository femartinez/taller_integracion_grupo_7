class ApiConsumerG2 < ActiveRecord::Base
    
    def self.create_group
        set_host
        uri = URI(@host+"/b2b/new_user")
        query = {"username" => "grupo7" , "password" => "grupo7"}
        resp = HTTParty.post(uri, :query => query)
        @response = JSON.parse(resp.body)
        token = @response['token']
        if token == nil
            p @response['message']
            token = get_token
        else
            token = @response['token']
        end
		return token
    end
    
    def self.get_token
        set_host
        uri = URI(@host+"/b2b/get_token")
        query = {"username" => "grupo7" , "password" => "grupo7"}
        resp = HTTParty.post(uri, :query => query)
        @response = JSON.parse(resp.body)
		return @response['token']
    end
    
    def self.create_order(order_id,client,provider,sku,delivery_date,quantity,price)
        set_host
        token = get_token
        uri = URI(@host+"/b2b/new_order")
        query = {"order_id" => order_id , "client" => client , "provider" => provider, "sku" => sku, "delivery_date" => delivery_date, "quantity" => quantity, "price" => price}
        header = { "Authorization-Token" => token}
        resp = HTTParty.post(uri, :query => query , :headers => header)
        @response = JSON.parse(resp.body)
		return @response
    end
    
    def self.accept_order(order_id)
        set_host
        token = get_token
        uri = URI(@host+"/b2b/order_accepted")
        query = {"order_id" => order_id}
        header = { "Authorization-Token" => token}
        resp = HTTParty.post(uri, :query => query, :headers => header)
        @response = JSON.parse(resp.body)
		return @response
    end
    
    def self.cancel_order(order_id)
        set_host
        uri = URI(@host+"/b2b/order_cancelled")
        token = get_token
        query = {"order_id" => order_id}
        header = { "Authorization-Token" => token}
        resp = HTTParty.post(uri, :query => query, :headers => header)
        @response = JSON.parse(resp.body)
		return @response
    end
    
    def self.reject_order(order_id)
        set_host
        uri = URI(@host+"/b2b/order_rejected")
        token = get_token
        query = {"order_id" => order_id}
        header = { "Authorization-Token" => token}
        resp = HTTParty.post(uri, :query => query, :headers => header)
        @response = JSON.parse(resp.body)
		return @response
    end
    
    def self.invoice_paid(invoice_id)
        set_host
        token = get_token
        uri = URI(@host+"/b2b/invoice_paid")
        query = {"invoice_id" => invoice_id}
        header = { "Authorization-Token" => token}
        resp = HTTParty.post(uri, :query => query, :headers => header)
        @response = JSON.parse(resp.body)
		return @response
    end
    
    def self.invoice_paid(invoice_id)
        set_host
        token = get_token
        uri = URI(@host+"/b2b/invoice_paid")
        query = {"invoice_id" => invoice_id}
        header = { "Authorization-Token" => token}
        resp = HTTParty.post(uri, :query => query, :headers => header)
        @response = JSON.parse(resp.body)
		return @response
    end
    
    def self.set_host
	    @host = "http://integra2.ing.puc.cl"
    end
end
