class Price < ActiveRecord::Base
  require 'json'
  
  # retorna el precio del producto para la fecha indicada, 
  # viene en formato float
  def self.get_price(sku , datetime)
    set_host
    uri = URI.parse(@host)
    query = { sku: sku, fecha: datetime }
    @response = HTTParty.get(uri, query: query)
    @response.parsed_response[0]['Precio'].to_f.ceil.to_i
  end

  def self.set_host
    if Rails.env = 'development'
        @host = 'http://chiri.ing.puc.cl/integra7/precio'
      else
        @host = 'http://moyas.ing.puc.cl/integra7/precio'
      end
  end
end
