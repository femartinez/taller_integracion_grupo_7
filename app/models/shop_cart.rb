class ShopCart < ActiveRecord::Base
  belongs_to :user
  has_many :line_items

  def calcular_total
    total = 0
    self.line_items.pending.each do |li|
      total += li.quantity * li.unit_price.to_i
    end
    total
  end

  def vaciar
    self.line_items.each do |li|
      li.status = 1
      li.save
    end
  end
end

	
