class Almacen < ActiveRecord::Base

  require 'uri'
  require 'base64'
  require 'hmac-sha1'
  require 'httparty'
  require 'json'

  has_many :items
  
  
  def self.save_almacenes(data)
    data.each do |d|
      p d
      a = Almacen.where(almacen_id: d['_id'])

      if a.first.nil?
        # Tengo que crear el almacen en la bd
        tipo = 'general'
        if d['pulmon']
          tipo = 'pulmon'
        elsif d['despacho']
          tipo = 'despacho'
        elsif d['recepcion']
          tipo = 'recepcion'
        end
        a_nuevo = Almacen.new(almacen_id: d['_id'],
                              tipo: tipo, totalSpace: d['totalSpace'],
                              usedSpace: d['usedSpace'])
        a_nuevo.save
      else
        # Actualizo los atributos del almacen para evitar inconsistencias
        a.first.assign_attributes(totalSpace: d['totalSpace'],
                                  usedSpace: d['usedSpace'])
        a.first.save
      end
    end
  end

  # Métodos para conectarse con la aplicacion JBOSS
  #  Sistema bodega
  def self.get_almacenes
      set_host
      uri = URI.parse(@host + '/almacenes')
      create_hash_sha1('GET')
      @response = HTTParty.get(uri, headers: @header)
      Almacen.save_almacenes(@response.parsed_response)
      @response
  end

  # Descripcion: Entrega una lista de skus que tienen stock en un almacen
  def self.get_sku(almacen_id)
    set_host
    uri = URI.parse(@host + '/skusWithStock')
    # Creamos el Hash
    create_hash_sha1('GET' + almacen_id)
    query = { almacenId: almacen_id }
    @response = HTTParty.get(uri, query: query, headers: @header)
    @response
  end
      
  # Descripcion: Devuelve todos los productos
  # que se encuentran guardados en un almacen
  # Agregue una parte adicional para que solo
  # se devuelvan los id's de los productos (Diego)
  def self.get_stock(almacen_id, sku)
    set_host
    uri = URI.parse(@host + '/stock')
    #Creamos el Hash
    create_hash_sha1('GET' + almacen_id + sku)
    query = { 'almacenId' => almacen_id, 'sku' => sku }
    @stock = HTTParty.get(uri, query: query, headers: @header)
    if @stock.parsed_response[0]
      create_or_update_item(almacen_id, @stock.parsed_response)
    end
    return @stock
  end

  def self.create_or_update_item(almacen_id, res)
    res.each do |item|
      p "Revisando los items"
      i = Item.where(item_id: item['_id']).first
      unless i
        p "Se creó un item"
        i = Item.new
        i.reserved = 0
        i.in_warehouse = 1
      end
      i.item_id = item['_id']
      almacen = Almacen.where(almacen_id: item['almacen']).last
      i.almacen = almacen
      i.sku = item['sku']
      i.tipo = 'output'
      i.tipo = 'input' if i.sku == 4 || i.sku == 7
      i.save
    end
  end
  
  def self.update_almacenes_stock(sku)
    get_almacenes
    almacenes = Almacen.all
    almacenes.each do |a|
      p a.almacen_id
      p sku
      Almacen.get_stock(a.almacen_id , sku)
    end
  end


  def self.get_total_stock(sku)
    total = 0
    Almacen.get_almacenes.each do |a|
      total += Almacen.get_stock(a["_id"], sku)
    end
    return total
  end


  # Descripcion: Mueve un producto de un almacen a otro,
  # siempre y cuando haya espacio en el almacen de destino
  def self.mover_stock(almacen_id, producto_id)
    set_host
    uri = URI.parse(@host + '/moveStock')
    body = { 'productoId' => producto_id, 'almacenId' => almacen_id }
    # Creamos el Hash
    create_hash_sha1('POST' + producto_id + almacen_id)
    @response = HTTParty.post(uri, body: body, headers: @header)
    #alm = Almacen.where(almacen_id: almacen_id).last
    #Item.where(item_id: producto_id).first.mover(alm) if @response.code == 200
    @response
  end
  # Descripcion: Mueve un producto de una bodega a otra,
  # siempre y cuando haya espacio en el almacen de
  # recepcion de la bodega de destino
  def self.mover_stock_bodega(almacen_id, producto_id)
    set_host
    uri = URI.parse(@host + '/moveStockBodega')
    body_parameters = { 'productoId' => producto_id, 'almacenId' => almacen_id }
    # Creamos el Hash
    create_hash_sha1('POST' + producto_id + almacen_id)
    @response = HTTParty.post(uri, body: body_parameters, headers: @header)
    #Item.where(item_id: producto_id).first.despachar if @response.code == 200
    if @response.code == 200
      i = Item.where(item_id: producto_id).first
      i.in_warehouse = 0
      i.save
    end
    @response
  end
  # Descripcion: Envia un stock a un cliente desde la bodega de despacho,
  # este se usa para pedidos FTP
  def self.despachar_stock(producto_id, direccion, precio, orden_id)
    set_host
    uri = URI.parse(@host + '/stock')
    body_parameters = { 'productoId' => producto_id, 'direccion' => direccion,
                        'precio' => precio, 'pedidoId' => orden_id
                      }
    # Creamos el hash
    create_hash_sha1('DELETE' + producto_id + direccion + precio + orden_id)
    @response = HTTParty.delete(uri, body: body_parameters, headers: @header)
    # Item.where(item_id: producto_id).first.despachar if @response.code == 200
    @response
  end

  # Recibe la cantidad exacta que se quiere producir
  
  def self.validar_stock_insumos_despacho(sku, cantidad)
    
    # primero hay que ver si es un producto primario o secundario
    Fabrica.array_formula_lote.each do |f|
      if f[:sku] == sku
        # es un producto secundario
        if Almacen.get_total_stock(f[:sku_insumo]) >= cantidad
          return true
        else
          return false
        end
      end
    end
    true
  end

  private

  #Creamos el hash y lo ponemos en la variable @hash
  def self.create_hash_sha1(string_1) 
    if Rails.env == 'development'
      @hash = Base64.encode64((HMAC::SHA1.new('2GfTmR7dWqSkTiJ') << string_1).digest).strip
    else
      @hash = Base64.encode64((HMAC::SHA1.new('M0aD0pJlFNrASW') << string_1).digest).strip
    end
    @header = { 'Authorization' => 'INTEGRACION grupo7:' + @hash }

  end

  def self.set_host
    if Rails.env == 'development'
      @host = "http://integracion-2015-dev.herokuapp.com/bodega"
    else
      @host = "http://integracion-2015-prod.herokuapp.com/bodega"
    end
  end

  def self.array_recepciones
    if Rails.env == 'development'
      @array_recepciones =
      [
        { group: 1, reception_id: '556489e6efb3d7030091bac2' },
        { group: 2, reception_id: '556489e7efb3d7030091bb7d' },
        { group: 3, reception_id: '556489e7efb3d7030091bc67' },
        { group: 4, reception_id: '556489e7efb3d7030091bd07' },
        { group: 5, reception_id: '556489e7efb3d7030091bdcc' },
        { group: 6, reception_id: '556489e7efb3d7030091bf2b' },
        { group: 7, reception_id: '556489e7efb3d7030091bf6f' },
        { group: 8, reception_id: '556489e7efb3d7030091bf89' }
      ]
    else
      @array_recepciones =
      [
        { group: 1, reception_id: '55648ae6f89fed030052500d' },
        { group: 2, reception_id: '55648ae6f89fed0300525061' },
        { group: 3, reception_id: '55648ae7f89fed030052512f' },
        { group: 4, reception_id: '55648ae6f89fed03005251d0' },
        { group: 5, reception_id: '55648ae7f89fed0300525219' },
        { group: 6, reception_id: '55648ae7f89fed0300525293' },
        { group: 7, reception_id: '55648ae7f89fed0300525369' },
        { group: 8, reception_id: '55648ae7f89fed0300525420' }
      ]
    end
  end
  def self.array_empresas
    @array_empresas =
    [
      {id: '1', nombre: 'Grupo1', direccion: 'Mohrenstraße 82, 10117 Berlin, Germany', servidor: 'http://integra1.ing.puc.cl/', pais: 'Alemania', continente: 'EUROPA' },
      {id: '2', nombre: 'Grupo2', direccion: 'Denezhnyy per., 9 стр. 1, Moscow, Russia', servidor: 'http://integra2.ing.puc.cl/', pais: 'Rusia', continente: 'ASIA' },
      {id: '3', nombre: 'Grupo3', direccion: 'Rigillis 15, 10674 Atenas', servidor: 'http://integra3.ing.puc.cl/', pais: 'Grecia', continente: 'EUROPA' },
      {id: '4', nombre: 'Grupo4', direccion: 'Nº2 Saleh Ayoub Suite 41, Zamalek, Cairo', servidor: 'http://integra4.ing.puc.cl/', pais: 'Egipto', continente: 'AFRICA' },
      {id: '5', nombre: 'Grupo5', direccion: '2290 Yan An West Road. Shanghai', servidor: 'http://integra5.ing.puc.cl/', pais: 'China', continente: 'ASIA' },
      {id: '6', nombre: 'Grupo6', direccion: 'Calle de Lagasca, 89, 29010 Madrid, Spain', servidor: 'http://integra6.ing.puc.cl/', pais: 'España', continente: 'EUROPA' },
      {id: '7', nombre: 'Grupo7', direccion: '10 Culgoa CircuitO\'Malley, australia', servidor: 'http://integra7.ing.puc.cl/', pais: 'Australia', continente: 'OCEANIA' },
      {id: '8', nombre: 'Grupo8', direccion: '169 Garsfontein Road,  Delmondo Office Park, Sudafrica', servidor: 'http://integra8.ing.puc.cl/', pais: 'Sudáfrica', continente: 'AFRICA' },
      {id: '9', nombre: 'Mayorista Vital', direccion: 'Balvanera Buenos Aires, Argentina', servidor: 'www.vital.com.ar', pais: 'Argentina', continente: 'AMERICA DEL SUR' },
      {id: '10', nombre: 'Atomo', direccion: 'Alvarez Condarco 740 Las Heras, Mendoza, Argentina', servidor: 'www.atomoconviene.com', pais: 'Argentina', continente: 'AMERICA DEL SUR' },
      {id: '11', nombre: 'Comercial S y P', direccion: 'Avda Cristobal Colon 3707 Santiago', servidor: 'https://plus.google.com/100984494934590358450/about?hl=en', pais: 'Chile', continente: 'AMERICA DEL SUR' },
      {id: '12', nombre: 'Mercado El Bosque', direccion: 'Republica de Chile 504 Jesús María, Peru', servidor: 'www.trome.pe', pais: 'Perú', continente: 'AMERICA DEL SUR' },
      {id: '13', nombre: 'SLUCKIS HNOS. S.A.', direccion: 'Arenal Grande 2193 Montevideo, Uruguay', servidor: 'www.sluckis.com.uy', pais: 'Uruguay', continente: 'AMERICA DEL SUR' },
      {id: '14', nombre: 'DMC', direccion: 'Anahí, Santa Cruz de la Sierra, Bolivia', servidor: 'http://www.dmc.bo/', pais: 'Bolivia', continente: 'AMERICA DEL SUR' },
      {id: '15', nombre: 'Fastrax S.A.', direccion: 'Tte. Fariña Nº 166 esq. Yegros', servidor: 'http://www.fastrax.com.py/', pais: 'Paraguay', continente: 'AMERICA DEL SUR' },
      {id: '16', nombre: 'MPS', direccion: '45 CC Monterrey Locales 326 y 327 Carrera 50 # 10, Medellín, Antioquia, Colombia', servidor: 'www.mps.com.co', pais: 'Colombia', continente: 'AMERICA DEL SUR' },
      {id: '17', nombre: 'Kode-tech', direccion: 'Av. Sanatorio del Ávila, Edif. Yacambú, Piso 3, Boleita Norte, Caracas, Venezuela.', servidor: 'http://www.kode-tech.com/', pais: 'Venezuela', continente: 'AMERICA DEL SUR' },
      {id: '18', nombre: 'MAMBO', direccion: 'Rua Deputado Lacerda Franco, 553 - Pinheiros São Paulo - SP, Brazil', servidor: 'www.mambo.com.br', pais: 'Brasil', continente: 'AMERICA DEL SUR' },
      {id: '19', nombre: 'DCM', direccion: 'Laguna de Mayrán 300 Anáhuac, Miguel Hidalgo, Ciudad de México, D.F., Mexico', servidor: 'www.dcm.com.mx', pais: 'México', continente: 'AMERICA DEL NORTE' },
      {id: '20', nombre: 'Greenfield USA', direccion: '5641 Dewey St Hollywood, FL, United States', servidor: 'www.greenfieldusaauto.com', pais: 'Estados Unidos', continente: 'AMERICA DEL NORTE' },
      {id: '21', nombre: 'TopTen', direccion: '448 S Hill St #712 Los Angeles, CA, United States', servidor: 'www.toptenwholesale.com', pais: 'Estados Unidos', continente: 'AMERICA DEL NORTE' }
    ]
  end

  def self.hash_min_stock
    @array_min = { "2" => { "cantidad" => 6012, "lote" => 1 }, 
      "3" => { "cantidad" => 1212, "lote" => 1 }, 
      "11" => { "cantidad" => 7212, "lote" => 900 }, 
      "40" => { "cantidad" => 3712, "lote" => 900 }, 
      "41" => { "cantidad" => 4812, "lote" => 200 } }
  end

  def self.set_min_stock(sku, min)
    unless @array_min[sku].nil?
      @array_min[sku]["cantidad"] = min
    end
  end

  def self.mantener_stock_minimo
    #Actualizar estado de cada almacen
    Almacen.get_almacenes

    @almacenes = Almacen.all
    #Productos que generamos nosotros:
    @min = Almacen.hash_min_stock
    @stock_productos = Hash.new

    @min.each_key do |key|
      @stock_productos[key] = 0
      @almacenes.each do |a|
        @stock_productos[key] += Almacen.get_stock(a.almacen_id,key).count
      end
    end
  #Ahora que tenemos el stock total de productos, hay que ver si cumplen el stock minimo.

    @stock_productos.each do |key, value|
      if value < @min[key]["cantidad"]
        # Hay que mandar a fabricar
        min_lotes = (Float(@min[key]["cantidad"] - value) / Float(@min[key]["lote"])).ceil.to_i
        Fabrica.producir(key, min_lotes)
      end
    end
  end

end
