class LineItem < ActiveRecord::Base
  belongs_to :shop_cart
  scope :pending, -> { where(status: 0) }
  scope :ready, -> { where(status: 1) }
end
