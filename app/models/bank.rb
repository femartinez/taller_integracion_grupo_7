class Bank < ActiveRecord::Base
  require 'uri'
  require 'httparty'
  require 'json'
  require 'date'

  #Obtener cartola
  def self.cartola(fecha_inicio, fecha_fin, id, limit)
    set_host
    set_header
    uri = URI(@host.to_s + '/cartola')
    query = ''
    if limit
      query = {"fecha_inicio"=> date_to_millis(fecha_inicio), "fecha_termino"=> date_to_millis(fecha_fin), "id"=>id, "limit"=>limit}
    else
      query = {"fecha_inicio"=> date_to_millis(fecha_inicio), "fecha_termino"=> date_to_millis(fecha_fin), "id"=>id, "limit"=>0}
    end
    @response = HTTParty.post(uri, query: query, headers: @header)
  end

  #Obtener cuenta
  #PI: Decidir que retornar
  def self.saldo(account_id)
    set_host
    set_header
    uri = URI(@host + '/cuenta/' + account_id)
    body_content = { 'Id' => account_id }
    @response = HTTParty.get(uri,:body => body_content.to_json,:headers => @header)
  end

  def self.cuenta(nro_grupo)
    cuenta_grupo
    @cuentas_grupos[nro_grupo - 1][:bank_account]
  end

  private

  def self.set_host
    if Rails.env == 'development'
      @host = ENV['HOST_BANCO']
    else
      @host = 'http://moyas.ing.puc.cl:8080/Integra7V2/webresources/banco'
    end
  end
  
  def self.set_header
    @header = { 'Content-Type' => 'application/xml' }
  end
  # Transforma una fecha a milisegundos desde la fecha
  def self.date_to_millis(fecha)
    fecha.strftime('%Q')
  end
   def self.set_cuenta_banco
    if Rails.env == 'development'
      @bank_account_id = '556489daefb3d7030091bab6'
    else
      @bank_account_id = '55648ad3f89fed0300525001'
    end
  end
  
  def self.cuenta_grupo
    if Rails.env = "development"
      @cuentas_grupos = [
      { group:1, bank_account: '556489daefb3d7030091bab3' },
      { group:2, bank_account: '556489daefb3d7030091bab5' },
      { group:3, bank_account: '556489daefb3d7030091bab7' },
      { group:4, bank_account: '556489daefb3d7030091bab9' },
      { group:5, bank_account: '556489daefb3d7030091bab2' },
      { group:6, bank_account: '556489daefb3d7030091bab4' },
      { group:7, bank_account: '556489daefb3d7030091bab6' },
      { group:8, bank_account: '556489daefb3d7030091bab8' }
      ]
    else
      @cuentas_grupos = [
        { group: 1, bank_account:'55648ad3f89fed0300524ffe' },  
        { group: 2, bank_account:'55648ad3f89fed0300525000' },
        { group: 3, bank_account:'55648ad3f89fed0300525002' },
        { group: 4, bank_account:'55648ad3f89fed0300525004' },
        { group: 5, bank_account:'55648ad3f89fed0300524ffd' },
        { group: 6, bank_account:'55648ad3f89fed0300524fff' },
        { group: 7, bank_account:'55648ad3f89fed0300525001' },
        { group: 8, bank_account:'55648ad3f89fed0300525003' }
      ]
    end
    
  end
end
