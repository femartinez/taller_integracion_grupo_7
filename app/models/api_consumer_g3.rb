class ApiConsumerG3 < ActiveRecord::Base
    
    #Modelo creado para consumir la api del grupo 3. 
    require 'uri'
    
    #Observación, los header no se deben enviar sino tira error
    #El único header que se necesita es el de authorization por token
    
    def self.create_group
        set_host
        uri = URI(@host+"/b2b/new_user/")
        #header = {'Content-Type' =>'application/json', 'Accept' => 'application/json'}
        body = {'username' => "grupo7" , 'password' =>"grupo7"}
        resp = HTTParty.put(uri,:body => body )
        @response = JSON.parse(resp.body)
        return @response
    end
    
    def self.get_token
        set_host
        uri = URI(@host+"/b2b/get_token/")
        #header = {'Content-Type' =>'application/json', 'Accept' => 'application/json'}
        body = {'username' => "grupo7" , 'password' =>"grupo7"}
        resp = HTTParty.post(uri,:body => body )
        @response = JSON.parse(resp.body)
        return @response['token']
    end
    
    def self.create_order(order_id)
        set_host
        token = get_token
        uri = URI(@host+"/b2b/new_order/")
        header = { "Authorization" => "Token "+token}
        body = {'order_id'=> order_id}
        resp = HTTParty.post(uri,:body => body, :headers => header)
        @response = JSON.parse(resp.body)
        return @response
    end
    
    #Para cuando aceptamos una orden de compra del grupo 
    def self.accept_order(order_id)
        set_host
        token = get_token
        uri = URI(@host+"/b2b/order_accepted/")
        header = { "Authorization" => "Token "+token}
        body = {'order_id'=> order_id}
        resp = HTTParty.post(uri,:body => body, :headers => header)
        @response = JSON.parse(resp.body)
        return @response
    end
    
    def self.cancel_order(order_id)
        set_host
        token = get_token
        uri = URI(@host+"/b2b/order_canceled/")
        header = { "Authorization" => "Token "+token}
        body = {'order_id'=> order_id}
        resp = HTTParty.post(uri,:body => body, :headers => header)
        @response = JSON.parse(resp.body)
        return @response
    end
    
    def self.reject_order(order_id)
        set_host
        token = get_token
        uri = URI(@host+"/b2b/order_rejected/")
        header = { "Authorization" => "Token "+token}
        body = {'order_id'=> order_id}
        resp = HTTParty.post(uri,:body => body, :headers => header)
        @response = JSON.parse(resp.body)
        return @response
    end
    
    def self.send_invoice(invoice_id)
        set_host
        token = get_token
        uri = URI(@host+"/b2b/invoice_created/")
        header = { "Authorization" => "Token "+token}
        body = {'invoice_id'=> invoice_id}
        resp = HTTParty.post(uri,:body => body, :headers => header)
        @response = JSON.parse(resp.body)
        return @response
    end
    
    def self.reject_invoice(invoice_id)
        set_host
        token = get_token
        uri = URI(@host+"/b2b/invoice_rejected/")
        header = { "Authorization" => "Token "+token}
        body = {'invoice_id'=> invoice_id}
        resp = HTTParty.post(uri,:body => body, :headers => header)
        @response = JSON.parse(resp.body)
        return @response
    end
    
    def self.invoice_paid(invoice_id)
        set_host
        token = get_token
        uri = URI(@host+"/b2b/invoice_paid/")
        header = { "Authorization" => "Token "+token}
        body = {'invoice_id'=> invoice_id}
        resp = HTTParty.post(uri,:body => body, :headers => header)
        @response = JSON.parse(resp.body)
        return @response
    end
    
    private
	def self.set_host
		@host = "http://integra3.ing.puc.cl"
	end
		
end
