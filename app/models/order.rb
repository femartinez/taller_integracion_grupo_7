class Order < ActiveRecord::Base
  require 'httparty'
  require 'json'
  require 'date'
  require 'uri'
  require 'base64'
  require 'hmac-sha1'
  require 'net/http'

  has_many :invoices
  has_many :items

  def self.save_oc(data)
    unless data.nil?
      orders = Order.where(:order_id => data["_id"])
      if orders.first.nil?
        #tengo que crear la orden en la bd
        o = Order.new(order_id: data["_id"], cliente: data["cliente"], proveedor: data["proveedor"], 
          sku: data["sku"], estado: data["estado"], fechaEntrega: data["fechaEntrega"],
          precioUnitario: data["precioUnitario"], cantidad: data["cantidad"],
          canal: data["canal"])
        o.save
      else
        #actualizo los atributos de la orden para evitar inconsistencias
        orders.first.assign_attributes(cliente: data["cliente"], proveedor: data["proveedor"], 
          sku: data["sku"], estado: data["estado"], fechaEntrega: data["fechaEntrega"],
          precioUnitario: data["precioUnitario"], cantidad: data["cantidad"],
          canal: data["canal"])
        orders.first.save
      end
    end 
  end

  # Metodo para actualizar una orden de compra,
  # requiere una variable @response seteada
  def self.update(r)
    o = Order.where(order_id: r['_id']).first
    o.estado = r['estado']
    o.cliente = r['cliente']
    o.proveedor = r['proveedor']
    o.cantidad = r['cantidad']
    o.fechaEntrega = r['fechaEntrega']
    o.notas = r['notas']
    o.precioUnitario = r['precioUnitario']
    o.sku = r['sku']
    o.canal = r['canal']
    o.order_id = r['_id']
    o.save
  end

  
  # Metdos para conexion con servidor de aplicaciones
  
  # Recibe parametro order_id
  # Actualiza la base de datos de ordenes de compra con la informacion
  # Devuelve la orden en la respuesta http
  def self.get(order_id)
    set_host
    set_header_oc
    uri = URI(@host + '/obtener/' + order_id)
    @response = HTTParty.get(uri, headers: @header)
    @r = JSON.parse @response.body
    Order.save_oc(@r[0]) if @response.code == 200
    @response
  end

  # Crea una orden de compra
  # El parametro notas no puede ser string
  def self.create_order(sku, cantidad, precio_unitario, fecha_entrega, proveedor, canal, notas)
    set_host
    set_header_oc
    uri = URI(@host + '/crear')
    query = { cliente: '7', proveedor: proveedor,
             canal: canal, cantidad: cantidad,
             notas: notas, sku: sku,
             fechaEntrega: date_to_millis(fecha_entrega),
             precioUnitario: precio_unitario
           }
    @response = HTTParty.put(uri, query: query, headers: @header)
    r = @response.parsed_response
    if @response.code == 200
      o = Order.new(cliente: r['cliente'], proveedor: r['proveedor'],
                    cantidad: r['cantidad'], estado: r['estado'],
                    fechaEntrega: r['fechaEntrega'], notas: r['notas'],
                    precioUnitario: r['precioUnitario'], sku: r['sku'],
                    canal: r['canal'], order_id: r['_id']
                   )
      o.save
    end
    @response
  end


  def self.receipt(order_id)
    set_host
    set_header_oc
    uri = URI(@host + '/recepcionar/' + order_id)
    query = { 'Id' => order_id }
    @response = HTTParty.post(uri, query: query, headers: @header)
    update(@response.parsed_response[0]) if @response.code == 200
    # Nos aseguramos que se devuelva @response
    @response
  end

  def self.reject(order_id, motivo)
    set_host
    set_header_oc
    uri = URI(@host + '/rechazar/' + order_id)
    query = { 'motivo' => motivo }
    @response = HTTParty.post(uri, query: query, headers: @header)
    p @response.parsed_response
    update(@response.parsed_response[0]) if @response.code == 200
    # Nos aseguramos que se devuelva @response
    @response
  end

  def self.cancel(order_id, motivo)
    set_host
    set_header_oc
    uri = URI(@host + '/anular/' + order_id)
    query = { 'motivo' => motivo }
    @response = HTTParty.delete(uri, query: query, headers: @header)
    update(@response.parsed_response[0]) if @response.code == 200
    # Nos aseguramos que se devuelva @response
    @response
  end

  # Transforma una fecha a milisegundos desde la fecha
  def self.date_to_millis(fecha)
    fecha.strftime('%Q')
  end

  private

  def self.set_host
    if Rails.env == 'development'
      @host = 'http://chiri.ing.puc.cl:8080/Integra7V2/webresources/oc'
    else
      @host = 'http://moyas.ing.puc.cl:8080/Integra7V2/webresources/oc'
    end
  end
  
  def self.set_header_oc
    @header = { 'Content-Type' => 'application/json' }
  end
  
  def self.create_example
    sku =11
    cantidad = 5
    precio_unitario = 1100
    fecha_entrega = DateTime.current + 6.days
    canal = 'b2b'
    notas = 13
    proveedor = 'grupo8'
    #Crear oc
    @create_order = Order.create_order(sku, cantidad, precio_unitario, fecha_entrega, proveedor, canal, notas)
    return @create_order
  end
end
