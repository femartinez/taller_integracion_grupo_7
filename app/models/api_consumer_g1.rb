class ApiConsumerG1 < ActiveRecord::Base
    #Modelo creado para consumir la api del grupo 1. 
    require 'uri'
    
    #Devuelve el token
    def self.create_group
        set_host
        uri = URI(@host+"/api/new_user")
        query = {"user" => "grupo7" , "pass" => "grupo7"}
        resp = HTTParty.post(uri, :query => query)
        @response = JSON.parse(resp.body)
        token = @response['token']
        if token == nil
            p @response['message']
            token = get_token
        else
            token = @response['token']
        end
		return token
    end
    
    def self.get_token
        set_host
        uri = URI(@host+"/api/get_token")
        query = {"user" => "grupo7" , "pass" => "grupo7"}
        resp = HTTParty.get(uri, :query => query)
        @response = JSON.parse(resp.body)
		return @response['token']
    end
    
    def self.create_order(order_id)
        set_host
        token = get_token
        uri = URI(@host+"/api/new_order")
        query = {"order_id" => order_id , "token" => token}
        resp = HTTParty.post(uri, :query => query)
        @response = JSON.parse(resp.body)
		return @response
    end
    
    #Para cuando aceptamos una orden de compra del grupo 1
    def self.accept_order(order_id)
        set_host
        uri = URI(@host+"/api/order_accepted")
        query = {"order_id" => order_id}
        resp = HTTParty.put(uri, :query => query)
        @response = JSON.parse(resp.body)
		return @response
    end
    
    def self.cancel_order(order_id)
        set_host
        uri = URI(@host+"/api/order_canceled")
        token = get_token
        query = {"order_id" => order_id , "token" => token }
        resp = HTTParty.delete(uri, :query => query)
        @response = JSON.parse(resp.body)
		return @response
    end
    
    def self.reject_order(order_id)
        set_host
        uri = URI(@host+"/api/order_rejected")
        token = get_token
        query = {"order_id" => order_id, "token" => token}
        resp = HTTParty.delete(uri, :query => query)
        @response = JSON.parse(resp.body)
		return @response
    end
    
    def self.send_invoice(invoice_id)
        set_host
        uri = URI(@host+"/api/new_invoice")
        query = {"invoice_id" => invoice_id}
        resp = HTTParty.post(uri, :query => query)
        @response = JSON.parse(resp.body)
		return @response
    end
    
    def self.reject_invoice(invoice_id)
        set_host
        uri = URI(@host+"/api/invoice_rejected")
        query = {"invoice_id" => invoice_id}
        resp = HTTParty.delete(uri, :query => query)
        @response = JSON.parse(resp.body)
		return @response
    end
    
    def self.invoice_paid(invoice_id)
        set_host
        uri = URI(@host+"/api/invoice_paid")
        token = get_token
        query = {"invoice_id" => invoice_id, "token" => token}
        resp = HTTParty.post(uri, :query => query)
        @response = JSON.parse(resp.body)
		return @response
    end
    
    private
	def self.set_host
		@host = "http://integra1.ing.puc.cl"
	end
		
end
