class Promotion < ActiveRecord::Base
  scope :active, ->(date) { where(date: date) }
  
  #  create_table "promotions", force: :cascade do |t|
  #   t.integer  "sku_id"
  #   t.integer  "price"
  #   t.datetime "starts_at"
  #   t.datetime "ends_at"
  #   t.datetime "created_at", null: false
  #   t.datetime "updated_at", null: false
  #   t.string   "code"
  # end
end
